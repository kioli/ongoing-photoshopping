package com.kioli.photoshopping.async;

/**
 * Interface with only a method run() that returns a generic T. Needed to be
 * implemented by customized class that need to run in a background thread
 */
public interface IRunnable<T> {
	T run();
}