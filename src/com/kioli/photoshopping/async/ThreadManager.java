package com.kioli.photoshopping.async;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.os.Handler;
import android.os.Looper;

/**
 * Singleton defining a thread manager in charge of controlling the execution of
 * background threads and using a fixed thread pool with 2 parallel threads
 * (customizable from within this enum
 */
public enum ThreadManager {

	INSTANCE(2);

	private final ExecutorService	executor;
	private final Handler	      handler;

	private ThreadManager(final int numThreads) {
		executor = Executors.newFixedThreadPool(numThreads);
		handler = new Handler(Looper.getMainLooper());
	}

	/**
	 * Takes an IRunnable type and runs it, providing the result back to the UI
	 * thread using both a Handler linked to the main looper and an IHandler to
	 * do the proper action related to the type of IRunnable
	 *
	 * @param runnable
	 *            - IRunnable object extending the method run and returning a
	 *            generic T
	 * @param callback
	 *            - IHandler callback notifying the proper object with the
	 *            IRunnable result (passed as generic T)
	 */
	public <T> void execute(final IRunnable<T> runnable, final IHandler<T> callback) {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				final T result = runnable.run();
				handler.post(new Runnable() {
					@Override
					public void run() {
						callback.onSuccess(result);
					}
				});
			}
		});
	}
}
