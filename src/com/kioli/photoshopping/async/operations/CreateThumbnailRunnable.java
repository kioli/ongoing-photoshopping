package com.kioli.photoshopping.async.operations;

import android.graphics.Bitmap;

import com.kioli.photoshopping.async.IRunnable;
import com.kioli.photoshopping.model.ModelThreadListResult;
import com.kioli.photoshopping.utils.ImageUtils;

/**
 * Class to create a thumbnail of a given image using the static method
 * {@link ImageUtils#resizeBitmapFromBitmap(Bitmap bitmap, int width, int height)}
 * 
 * @param photoPath
 *            - Path where the Bitmap of which we want a thumbnail is stored
 * @param width
 *            - desired width of the thumbnail
 * @param height
 *            - desired height of the thumbnail
 * @param id
 *            - id of the thread (used when you have a list and need to
 *            associate the result with the object
 * @return Bitmap - the resulting thumbnail
 */
public class CreateThumbnailRunnable implements
		IRunnable<ModelThreadListResult> {

	private String photoPath;
	private int width;
	private int height;
	private int id;

	public CreateThumbnailRunnable(final String photoPath, final int width,
			final int height, final int id) {
		this.photoPath = photoPath;
		this.width = width;
		this.height = height;
		this.id = id;
	}

	@Override
	public ModelThreadListResult run() {
		ModelThreadListResult model = new ModelThreadListResult();
		model.position = id;
		model.bitmap = ImageUtils.resizeBitmapFromAbsolutePath(photoPath, width, height);
		return model;
	}
}
