package com.kioli.photoshopping.async.operations;

import java.util.ArrayList;

import org.w3c.dom.Document;

import com.google.android.gms.maps.model.LatLng;
import com.kioli.photoshopping.async.IRunnable;
import com.kioli.photoshopping.gps.GMapV2GetRouteDirection;

/**
 * Class to find the distance between two points on the map
 * 
 * @param LatLng
 *            - Object storing longitude and latitude of the point to find the distance from
 * @param LatLng
 *            - Object storing longitude and latitude of the point tofind the distance to
 */
public class FindPathRunnable implements IRunnable<ArrayList<LatLng>> {

	private LatLng pointFrom;
	private LatLng pointTo;
	private String transportation;

	public FindPathRunnable(final LatLng point1, final LatLng point2, final String transportation) {
		this.pointFrom = point1;
		this.pointTo = point2;
		this.transportation = transportation;
	}

	@Override
	public ArrayList<LatLng> run() {
		GMapV2GetRouteDirection v2GetRouteDirection = new GMapV2GetRouteDirection();
		Document document = v2GetRouteDirection.getDocument(pointFrom, pointTo, transportation);
		return v2GetRouteDirection.getDirection(document);
	}
}
