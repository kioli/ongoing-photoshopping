package com.kioli.photoshopping.async.operations;

import android.graphics.Bitmap;

import com.kioli.photoshopping.async.IRunnable;
import com.kioli.photoshopping.utils.ImageUtils;

/**
 * Class to crop the image file of the photo just shot using the static method
 * {@link ImageUtils#cropBitmap(Bitmap bitmap, int newDimension) and passing
 * optional dimension for a furthermore shrinking}
 *
 * @param photoFile
 *            - object holding reference to the File where the bitmap to
 *            manipulate is stored
 * @return Bitmap of the photo just shot
 */
public class CropPhotoRunnable implements IRunnable<Bitmap> {

	private final String	filePath;
	private final int	 newDim;

	public CropPhotoRunnable(final String path, final int newDim) {
		this.filePath = path;
		this.newDim = newDim;
	}

	@Override
	public Bitmap run() {
		final Bitmap bmp = ImageUtils.getBitmapFromFile(filePath);
		return ImageUtils.cropBitmap(bmp, newDim);
	}
}
