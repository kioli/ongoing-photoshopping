package com.kioli.photoshopping.async.operations;

import java.io.File;

import android.graphics.Bitmap;

import com.kioli.photoshopping.async.IRunnable;
import com.kioli.photoshopping.utils.ImageUtils;

/**
 * Class to store the image file of the photo just shot using the static method
 * {@link ImageUtils#resizeBitmapFromFile(Bitmap photo, File photoFile)}
 *
 * @param photo
 *            - Bitmap that need to be saved on disk
 * @param photoFile
 *            - File that is gonna store the image
 */
public class StoreItemRunnable implements IRunnable<Boolean> {

	private final Bitmap	photo;
	private final File	 photoFile;

	public StoreItemRunnable(final Bitmap photo, final File file) {
		this.photo = photo;
		this.photoFile = file;
	}

	@Override
	public Boolean run() {
		return ImageUtils.storeBitmapInFile(photo, photoFile);
	}
}
