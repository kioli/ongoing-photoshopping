package com.kioli.photoshopping.async.operations;

import android.content.Context;

import com.kioli.photoshopping.async.IRunnable;
import com.kioli.photoshopping.gps.GpsTracker;

/**
 * Class to retrieve the address using the static method
 * {@link GpsTracker#getAddressFromLocation(Context context)}
 * 
 * @param context
 *            - Context of the calling activity
 */
public class GetAddressRunnable implements IRunnable<String> {

	private final Context	ctx;

	public GetAddressRunnable(final Context ctx) {
		this.ctx = ctx;
	}

	@Override
	public String run() {
		return GpsTracker.getAddressFromLocation(ctx);
	}
}
