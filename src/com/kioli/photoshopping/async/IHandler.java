package com.kioli.photoshopping.async;

/**
 * Interface needed to be implemented by customized handlers used to handle the
 * result from a customized background thread
 */
public interface IHandler<T> {
	void onSuccess(T result);
}
