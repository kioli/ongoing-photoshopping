package com.kioli.photoshopping.db;

import static com.kioli.photoshopping.db.dao.IObjectDao.COL_ID;
import static com.kioli.photoshopping.db.dao.IObjectDao.COL_NAME;
import static com.kioli.photoshopping.db.dao.IObjectDao.TABLE_NAME;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kioli.photoshopping.db.dao.IObjectDao;
import com.kioli.photoshopping.model.ModelItemShopping;
import com.kioli.photoshopping.utils.DbUtils;

final public class ObjectHelper {
    private static final String SQL_GET_ALL         = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + COL_ID + " DESC";
    private static final String SQL_GET_BY_NAME  	= "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_ID + " = ? AND " + COL_NAME + " = ?";
    private static final String SQL_GET_MAX_COUNTER = "SELECT MAX(" + COL_ID + ") FROM " + TABLE_NAME;

    public static int getObjectCount(final ObjectDbHelper dbhelper) {
        final SQLiteDatabase db = dbhelper.getDatabase();
        return DbUtils.getRecordsCount(db, TABLE_NAME);
    }

    public static List<ModelItemShopping> getAllObjects(final ObjectDbHelper dbhelper) {
        final List<ModelItemShopping> res = new ArrayList<ModelItemShopping>();

        final SQLiteDatabase db = dbhelper.getDatabase();
        final IObjectDao dao  = dbhelper.getObjectDao();

        Cursor cursor = null;
        try {
            cursor = db.rawQuery(SQL_GET_ALL, null);
            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    final ModelItemShopping object = dao.read(cursor);
                    res.add(object);
                    cursor.moveToNext();
                }
            } else {
                /* recordset was empty */
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return res;
    }

    public static ModelItemShopping getObjectByName(final ObjectDbHelper dbhelper, final long id, final String name) {
        final String sql = SQL_GET_BY_NAME;
        final String[] args = new String[] {String.valueOf(id), String.valueOf(name)};
        final ModelItemShopping object = loadOneObject(dbhelper, sql, args);
        return object;
    }

    private static ModelItemShopping loadOneObject(final ObjectDbHelper dbhelper, final String sql, final String[] args) {
        final SQLiteDatabase db = dbhelper.getDatabase();
        final IObjectDao dao  = dbhelper.getObjectDao();

        ModelItemShopping object = null;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, args);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                object = dao.read(cursor);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return object;
    }

    public static void saveOrUpdate(final ObjectDbHelper dbhelper, final ModelItemShopping object) {
        final SQLiteDatabase db = dbhelper.getDatabase();

        final IObjectDao dao = dbhelper.getObjectDao();
        final String tableName = TABLE_NAME;
        dao.ensureWritableTable(db, tableName, false);

        final long maxCounter = getMaxCounter(dbhelper);
        object.id = maxCounter + 1;

        final ContentValues args = new ContentValues();
        dao.write(object, args);

        final ModelItemShopping exists = getObjectByName(dbhelper, object.id, object.name);
        if (exists == null) {
            /* save */
            db.insert(tableName, null, args);
        } else {
            /* update */
            final String where = COL_NAME + " = " + object.name + " and " + COL_ID + " = " + object.id;
            db.update(tableName, args, where, null);
        }
    }

    private static long getMaxCounter(final ObjectDbHelper dbhelper) {
        final int cnt;
        Cursor counter = null;
        try {
            final SQLiteDatabase db = dbhelper.getDatabase();
            counter = db.rawQuery(SQL_GET_MAX_COUNTER, null);
            if (counter.moveToFirst()) {
                cnt = counter.getInt(0);
            } else {
                /* recordset was empty */
                cnt = 0;
            }
        } finally {
            if (counter != null) counter.close();
        }

        return cnt;
    }

    public static void removeObject(final ObjectDbHelper dbhelper, final ModelItemShopping object) {
        final SQLiteDatabase db = dbhelper.getDatabase();
        final String where = /*COL_NAME + " = " + object.name + " and " +*/ COL_ID + " = " + object.id;
        db.delete(TABLE_NAME, where, null);
    }

    private ObjectHelper() { }
}
