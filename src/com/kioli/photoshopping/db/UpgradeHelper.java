package com.kioli.photoshopping.db;

import static com.kioli.photoshopping.db.ObjectDbVersion.DATABASE_VERSION_1;
import static com.kioli.photoshopping.db.ObjectDbVersion.daoForVersion;
import static com.kioli.photoshopping.db.dao.IObjectDao.COL_DATE;
import static com.kioli.photoshopping.db.dao.IObjectDao.TABLE_NAME;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kioli.photoshopping.db.dao.IObjectDao;
import com.kioli.photoshopping.model.ModelItemShopping;
import com.kioli.photoshopping.utils.DbUtils;

public class UpgradeHelper {

    private static final String SQL_GET_ALL_1 = "select * from " + TABLE_NAME + " order by " + COL_DATE + " desc";
    
    public static void dropObjectsTable(final SQLiteDatabase db) {
        db.execSQL(DbUtils.makeDropSql(TABLE_NAME));
    }

    public static void createObjectsTable(final SQLiteDatabase db, final int version) {
        final IObjectDao newDao  = daoForVersion(version);
        newDao.ensureWritableTable(db, TABLE_NAME, true);
        newDao.ensureIndexes(db, TABLE_NAME);
    }

    public static void upgradeObjectsFrom(final SQLiteDatabase db,final int oldVersion, final int newVersion) {
        final IObjectDao oldDao  = daoForVersion(oldVersion);
        final IObjectDao newDao  = daoForVersion(newVersion);

        List<ModelItemShopping> res = null;
        switch (oldVersion) {
        case DATABASE_VERSION_1:
            // First upgrade must be done using a particular sorting. The column we are sorting on might not be there for other DAO versions
            res = loadObjects(db, SQL_GET_ALL_1, null, oldDao);
            updateObjectLisToVersion2(res);
            updateObjectLisToVersion3(res);
            /* .. updateObjectsListToVersion4(res)...
             * Place here the Object update method for new Object DB versions
             */
            break;
        default:

            break;
        }

        /* Drop the old table and create the new one. */
        newDao.ensureWritableTable(db, TABLE_NAME, true);

        saveObjects(db, TABLE_NAME, res, newDao);

        newDao.ensureIndexes(db, TABLE_NAME);

    }

    private static void updateObjectLisToVersion2(final List<ModelItemShopping> res){
        final int max = res.size();
        int counter = max;
        for (int i = 0; i < max; i++) {
            final ModelItemShopping object = res.get(i);
            object.id = counter;
            --counter;
        }
    }

    private static void updateObjectLisToVersion3(final List<ModelItemShopping> res){
//        for (final ItemShoppingModel object : res) {
//            /* Introduce the image data. Clearing it triggers fetching the image after the first login. */
//            object.setImageData(null);
//            /* Remove 'photo'. */
//            object.photo     = null;
//        }
    }


    private static List<ModelItemShopping> loadObjects(final SQLiteDatabase db, final String sql, final String[] args,
                    final IObjectDao dao) {

        final List<ModelItemShopping> res = new ArrayList<ModelItemShopping>();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, args);
            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    final ModelItemShopping object = dao.read(cursor);
                    res.add(object);
                    cursor.moveToNext();
                }
            } else {
                /* recordset was empty */
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return res;
    }

    private static void saveObjects(final SQLiteDatabase db, final String tableName, final List<ModelItemShopping> res, final IObjectDao dao) {
        if (res.isEmpty()) return;

        try {
            db.beginTransaction();
            final ContentValues args = new ContentValues();
            for (final ModelItemShopping object : res) {
                dao.write(object, args);
                db.insert(tableName, null, args);
                args.clear();
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }
}