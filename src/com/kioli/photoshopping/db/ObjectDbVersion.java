package com.kioli.photoshopping.db;

import com.kioli.photoshopping.db.dao.IObjectDao;
import com.kioli.photoshopping.db.dao.ObjectDAO;

/**
 * HOW IT WORKS:<br>
 * ======================================<br>
 * When creating a database for the first time your {@link ObjectDbHelper} provides CURRENT_VERSION as a version.
 * That value gets recorded in the database file. The hardcoded value of CURRENT_VERSION stays in the APK. For now
 * they match.<br><br>
 *
 * If a change to the DB schema is made for the new release you increment the CURRENT_VERSION in your code. When the
 * new release is installed, it will open the database. But now it will find that the value hardcoded in the APK is
 * different from the value recorded in DB file. This will trigger the upgrade scenario. After the upgrade completes
 * the new CURRENT_VERSION value is saved to the DB file.<br><br>
 *
 * So, important things are:<br><br>
 *    1) When changing the schema add a new DATABASE_VERSION_X constant. Assign it to the CURRENT_VERSION.<br>
 *    2) Once added a DATABASE_VERSION_X constant NEVER CHANGE IT EVERAFTER.<br>
 *    3) Implement new clause for your DATABASE_VERSION_X in the ObjectDbHelper#onUpdate method.<br>
 *
 * @see ObjectDbHelper
 * @see UpgradeHelper
 * @see ObjectDao1
 * @see etc.
 */
final public class ObjectDbVersion {
    public static final int DATABASE_VERSION_1 = 1;
    /* Add a new DATABASE_VERSION_X here, but never ever change those above. */

    public static final int CURRENT_VERSION = DATABASE_VERSION_1;

    public static IObjectDao daoForVersion(final int version) {
        switch (version) {
            case DATABASE_VERSION_1: return new ObjectDAO();
            /* Add DAO for a new version here. */

            default: throw new UnsupportedOperationException("No Object DAO class provided for database version " + version);
        }
    }

    private ObjectDbVersion() { }
}
