package com.kioli.photoshopping.db;

import java.util.List;

import com.kioli.photoshopping.PhotoShopApp;
import com.kioli.photoshopping.model.ModelItemShopping;

public class ObjectManager {

	private static ModelItemShopping currentObject;

	public static void saveOrUpdate(final ModelItemShopping object) {
	    ObjectHelper.saveOrUpdate(getObjectDbHelper(), object);
	}

	public static List<ModelItemShopping> getAllObjects() {
		return ObjectHelper.getAllObjects(getObjectDbHelper());
	}

	public static void removeObject(final ModelItemShopping object) {
		ObjectHelper.removeObject(getObjectDbHelper(), object);
	}

	public static int getObjectCount() {
		return ObjectHelper.getObjectCount(getObjectDbHelper());
	}

	public static ModelItemShopping getObjectByName(final long id, final String name) {
	    return ObjectHelper.getObjectByName(getObjectDbHelper(), id, name);
	}

	public static void setCurrentObject(final ModelItemShopping object) {
	    currentObject = object;
	}

	public static void resetObject(){
	    currentObject = null;
	}

	public static ModelItemShopping getCurrentObject() {
       return currentObject;
    }

    private static ObjectDbHelper getObjectDbHelper(){
        return ObjectDbHelper.getHelper(PhotoShopApp.getContext());
    }
}
