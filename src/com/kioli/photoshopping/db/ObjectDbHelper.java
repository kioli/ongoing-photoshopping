package com.kioli.photoshopping.db;

import static com.kioli.photoshopping.db.ObjectDbVersion.CURRENT_VERSION;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kioli.photoshopping.db.dao.IObjectDao;

final public class ObjectDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "photoshopping.db";

    private static ObjectDbHelper helper;

    private IObjectDao            daoObject;
    private boolean               isDaoCreated;


    public static ObjectDbHelper getHelper(final Context context) {
        if (helper == null) {
            helper = new ObjectDbHelper(context.getApplicationContext());
        }
        return helper;
    }

    private ObjectDbHelper(final Context context) {
        /*
         * WARNING: call this constructor only with application context! The context
         * is stored in the baseclass' private field mContext, thus making a context
         * leak if anything other than application context is passed here!
         */
        super(context, DATABASE_NAME, null, CURRENT_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase db) {
        UpgradeHelper.createObjectsTable(db, CURRENT_VERSION);
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
        switch (oldVersion) {
//        case DATABASE_VERSION_1:
//            UpgradeHelper.upgradeObjectsFrom(db,oldVersion,newVersion);
//            break;
        default:
            dropAndCreate(db);
            break;
        }
    }

    private void dropAndCreate(final SQLiteDatabase db) {
        UpgradeHelper.dropObjectsTable(db);
        onCreate(db);
    }

    public SQLiteDatabase getDatabase() {
        return getWritableDatabase();
    }

    /** This method makes sure that all DAO objects get the same key and cipher when instantiated. */
    private void ensureDaoObjects() {
        if (!isDaoCreated) {
            daoObject = ObjectDbVersion.daoForVersion(CURRENT_VERSION);
            /* Add creating more DAO objects here */

            isDaoCreated = true;
        }
    }

    public IObjectDao getObjectDao () {
        ensureDaoObjects();
        return daoObject;
    }
}