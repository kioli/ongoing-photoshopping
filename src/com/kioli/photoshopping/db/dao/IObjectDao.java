package com.kioli.photoshopping.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kioli.photoshopping.model.ModelItemShopping;

public interface IObjectDao {
    public static final String TABLE_NAME      	= "photoshopping";

    public static final String COL_ID	     	= "_id";
    public static final String COL_NAME  		= "name";
    public static final String COL_PRICE	    = "price";
    public static final String COL_CURRENCY	    = "currency";
    public static final String COL_LONGITUDE    = "longitude";
    public static final String COL_LATITUDE     = "latitude";
    public static final String COL_ADDRESS      = "address";
    public static final String COL_DATE        	= "date";
	public static final String COL_IMG_PATH		= "imgpath";

    public void ensureWritableTable(SQLiteDatabase db, String tableName, boolean b);
    public void ensureIndexes(SQLiteDatabase db, String tableName);

    public ModelItemShopping read(Cursor cursor);
    public void write(ModelItemShopping mutation, ContentValues values);
}
