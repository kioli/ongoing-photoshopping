package com.kioli.photoshopping.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kioli.photoshopping.model.ModelItemShopping;
import com.kioli.photoshopping.utils.DbUtils;
import com.kioli.photoshopping.utils.amount.Amount;

public class ObjectDAO implements IObjectDao {

    private static String SCHEMA =
        COL_ID     		+ " INTEGER PRIMARY KEY, " +
        COL_NAME        + " TEXT, " +
        COL_CURRENCY 	+ " TEXT, " +
        COL_PRICE	  	+ " TEXT, " +
        COL_LATITUDE  	+ " DOUBLE PRECISION, " +
        COL_LONGITUDE   + " DOUBLE PRECISION, " +
        COL_ADDRESS     + " TEXT, " +
        COL_IMG_PATH 	+ " TEXT, " + 
        COL_DATE        + " TEXT, " +
        " UNIQUE("+ COL_ID +")";

    private static final String INDEX_NAME   = "counter_index";
    private static final String INDEX_FIELDS = COL_ID;

    @Override
    public void ensureWritableTable(final SQLiteDatabase db, final String tableName, final boolean clean) {
        DbUtils.ensureWritableTable(db, tableName, SCHEMA, clean);
    }

    @Override
    public void ensureIndexes(final SQLiteDatabase db, final String tableName) {
        DbUtils.ensureIndexOnTable(db, tableName, INDEX_NAME, INDEX_FIELDS);
    }

    @Override
    public ModelItemShopping read(final Cursor cursor) {
        final ModelItemShopping item = new ModelItemShopping();

        item.id				= cursor.getInt(0);
        item.name			= cursor.getString(1);
        item.currency		= cursor.getString(2);
        item.price			= Amount.fromStringValue(cursor.getString(3), item.currency);
        item.latitude		= cursor.getDouble(4);
        item.longitude		= cursor.getDouble(5);
        item.address		= cursor.getString(6);
        item.imgpath		= cursor.getString(7);
		item.date 			= cursor.getString(8);
//        final byte[] data   = cursor.getBlob(7);
//        item.setImageData(data);

        return item;
    }

    @Override
    public void write(final ModelItemShopping item, final ContentValues args) {
        args.put(COL_ID,		item.id);
        args.put(COL_NAME,      item.name);
        args.put(COL_CURRENCY,  item.currency);
        args.put(COL_PRICE,     item.price.toString());
        args.put(COL_LATITUDE,  item.latitude);
        args.put(COL_LONGITUDE, item.longitude);
        args.put(COL_ADDRESS, 	item.address);
		args.put(COL_IMG_PATH,  item.imgpath);
        args.put(COL_DATE,      item.date);
//        args.put(COL_IMAGE,     item.imageData);
    }
}
