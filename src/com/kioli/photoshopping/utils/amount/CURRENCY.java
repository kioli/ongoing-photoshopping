package com.kioli.photoshopping.utils.amount;

import java.util.HashMap;

import com.kioli.photoshopping.PhotoShopApp;

import android.util.Log;

public enum CURRENCY implements ICurrency {
    /* IF YOU DON'T SEE CLEAR SYMBOLS SWITCH THE ENCODING IN YOUR PROJECT TO UTF-8 */
    EURO              ("Euro", "€",   false),
    BRITISH_POUND     ("British pound", "£",   false),
    US_DOLLAR         ("American dollar", "$",   false);

    private final String             code;
    private final String             symbol;
    private final boolean            symbolAtRight;

    private static HashMap<String, CURRENCY> sMap;

    private CURRENCY(final String code, final String symbol, final boolean symbolAtRight) {
        this.code   = code;
        this.symbol = symbol;
        this.symbolAtRight = symbolAtRight;
    }

    private static HashMap<String, CURRENCY> ensureMap() {
        if (sMap == null) {
            sMap = new HashMap<String, CURRENCY>();
            for (final CURRENCY  item : CURRENCY.values()) {
                sMap.put(item.code, item);
            }
        }

        return sMap;
    }

    public static CURRENCY fromCodeValue(final String value) {
        final HashMap<String, CURRENCY> map = ensureMap();
        final CURRENCY item = map.get(value);

        if (item != null) {
            return item;
        } else {
        	Log.e(PhotoShopApp.TAG, "No currency recognized. Fallback to default EURO");
            return CURRENCY.EURO;
        }
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getSymbol() {
        return symbol;
    }

    @Override
    public boolean isSymbolRightAligned() {
        return symbolAtRight;
    }
}
