package com.kioli.photoshopping.utils.amount;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.text.InputFilter;
import android.text.Spanned;

public class AmountUtils {

	public static String toStringWithCurrency(final Amount amount) {
        final StringBuilder sb = new StringBuilder();
        final ICurrency currency = amount.getCurrency();
        final String symbol = currency.getSymbol();
        final boolean hasSymbol = (symbol != null && symbol.length() > 0);

        if (hasSymbol) {
            if (currency.isSymbolRightAligned()) {
                sb.append(amount.toString());
                sb.append(' ').append(symbol);
            } else {
                sb.append(symbol).append(' ');
                sb.append(amount.toString());
            }
        } else {
        	sb.append(amount.toString());
        }

        return sb.toString();
    }

	public static class AmountInputFilter implements InputFilter {

		Pattern	mPattern;

		public AmountInputFilter(final int digitsBeforeZero) {
			mPattern = Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1) + "}+((\\.[0-9]{0,1})?)||(\\.)?");
		}

		@Override
		public CharSequence filter(final CharSequence source, final int start, final int end, final Spanned dest, final int dstart,
		        final int dend) {
			final Matcher matcher = mPattern.matcher(dest);
			if (!matcher.matches()) {
				return "";
			}
			return null;
		}
	}

    /** Hide constructor for utility classes. */
	private AmountUtils() {}
}
