package com.kioli.photoshopping.utils.amount;

public interface ICurrency {
    public String getCode();
    public String getSymbol();
    public boolean isSymbolRightAligned();
}
