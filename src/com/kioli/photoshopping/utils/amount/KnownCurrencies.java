package com.kioli.photoshopping.utils.amount;

import java.util.HashMap;


public class KnownCurrencies {
    public static final String EURO = CURRENCY.EURO.getCode();

    private static final HashMap<String, ICurrency> sMap;

    static {
        sMap = new HashMap<String, ICurrency>();
        for (final CURRENCY c : CURRENCY.values()) {
            addKnownCurrency(c);
        }
    }

    public static void addUnknownCurrency(final String code) {
        if (sMap.containsKey(code) == false) {
            addKnownCurrency(new UnknownCurrency(code));
        }
    }

    public static void addKnownCurrency(final ICurrency currency) {
        sMap.put(currency.getCode(), currency);
    }

    public static ICurrency getCurrency(final String code) {
        return sMap.get(code);
    }


    private static final class UnknownCurrency implements ICurrency {
        private final String code;

        private UnknownCurrency(final String code) {
            super();
            this.code = code;
        }

        @Override
        public String getCode() {
            return code;
        }

        @Override
        public String getSymbol() {
            return code;
        }

        @Override
        public boolean isSymbolRightAligned() {
            return false;
        }

        @Override
        public String toString() {
            return "UnknownCurrency [code=" + code + "]";
        }
    }
}
