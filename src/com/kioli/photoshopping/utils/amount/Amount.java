package com.kioli.photoshopping.utils.amount;

import android.os.Parcel;

public class Amount implements Comparable<Amount>, Cloneable {
    
    public static final String   DECIMAL_DIVIDER    = ",";

    private long                 value;
    private final String         currencyCode;

    public Amount(final long value, final String currencyCode) {
        if (currencyCode == null)
            throw new IllegalArgumentException("Currency null is not allowed");

        /* Ensure that the currency is known when creating an amount that uses it */
        KnownCurrencies.addUnknownCurrency(currencyCode);

        this.value = value;
        this.currencyCode = currencyCode;
    }
    
    public static Amount fromStringValue(final String stringValue, final String currencyCode) throws NumberFormatException {
        if (stringValue == null) throw new IllegalArgumentException("stringValue was null");
        if (currencyCode == null) throw new IllegalArgumentException("currencyCode was null");

        final boolean hasDecimal;

        final int pos = stringValue.indexOf(DECIMAL_DIVIDER);
        if (pos > 0) {
            hasDecimal = true;
        } else {
            hasDecimal = false;
        }

        final StringBuffer amountValue = new StringBuffer(stringValue.length() + 2);

        amountValue.append(stringValue);

        if (hasDecimal == false) {
            /* add missing "00" for cents */
            amountValue.append("00");
        } else {
            /* remove the decimal dot */
            amountValue.deleteCharAt(pos);
            /* add "0" if only one decimal position is present, e.g. "2.5" -> "2.50" */
            final int numdec = stringValue.length() - pos - 1;
            if (numdec < 1) {
                throw new NumberFormatException("Too few digits for cents.");
            } else if (numdec == 1) {
                amountValue.append('0');
            } else {
                if (numdec > 2) {
                    throw new NumberFormatException("Too many digits for cents.");
                }
            }
        }

        final long longVal = Long.parseLong(amountValue.toString());

        return new Amount(longVal, currencyCode);
    }

    /** Copy constructor. */
    public Amount(final Amount balance) {
        this(balance.value, balance.currencyCode);
    }

    public void setAmount(final Amount that) {
        this.value = that.value;
    }

    public long getEuros() {
        return getEuros(false);
    }

    public long getEuros(final boolean signed) {
        if (signed) {
            return value / 100;
        } else {
            return Math.abs(value / 100);
        }
    }

    public int getCents() {
        return getCents(false);
    }

    private int getCents(final boolean signed) {
        if (signed) {
            return (int) (value % 100);
        } else {
            return (int) Math.abs(value % 100);
        }
    }

    @Override
    public int compareTo(final Amount another) {
        assertSameCurrency(another);

        if (value > another.value)
            return 1;
        else if (value == another.value)
            return 0;
        else
            return -1;
    }

    public void setLongValue(final long value) {
        this.value = value;
    }

    public long getLongValue() {
        return value;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public ICurrency getCurrency() {
        return KnownCurrencies.getCurrency(currencyCode);
    }

    public boolean isPositive() {
        return (value > 0);
    }

    public boolean isZero() {
        return (value == 0);
    }

    public boolean isNegative() {
        return (value < 0);
    }

    @Override
    public String toString() {
        final int cents = getCents();
        return String.valueOf(getEuros()) + DECIMAL_DIVIDER + ((cents > 9) ? String.valueOf(cents) : "0" + cents);
    }

    public String toStringWithCurrency() {
    	return getCurrency().getSymbol() + " " + toString();
    }
    
    /** Remove the sigh. Negative values become positive. Positive values stay positive. */
    public void flatten() {
        value = Math.abs(value);
    }

    /** Invert the sigh. Positive values become negative.  Negative values become positive. */
    public void invert() {
        value = -value;
    }

    public void add(final Amount extra) {
        assertSameCurrency(extra);
        this.value = this.value + extra.getLongValue();
    }

    public void subtract(final Amount extra) {
        assertSameCurrency(extra);
        this.value = this.value - extra.getLongValue();
    }

    private boolean isSameCurrency(final Amount other) {
        final String thisCurrency = this.getCurrencyCode();
        final String thatCurrency = other.getCurrencyCode();
        return thisCurrency.equals(thatCurrency);
    }

    private void assertSameCurrency(final Amount other) {
        if (isSameCurrency(other) == false) {
        	throw new IllegalStateException("CURRENCY DOES NOT MATCH. " +
            		"You tried an operation with two Amounts which have different currencies. " +
            		"Automatic conversion is not supported.");
        }
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Amount other = (Amount) obj;
        if (currencyCode == null) {
            if (other.currencyCode != null)
                return false;
        } else if (!currencyCode.equals(other.currencyCode))
            return false;
        if (value != other.value)
            return false;
        return true;
    }
    
    public static void writeToParcel(final Parcel dest, final Amount balanceAmount) {
        if (balanceAmount != null) {
            dest.writeInt(1);
            dest.writeLong(balanceAmount.getLongValue());
            dest.writeString(balanceAmount.currencyCode);
        } else {
            dest.writeInt(0);
        }
    }

    public static Amount readFromParcel(final Parcel in) {
        final boolean amountPresent = (in.readInt() == 1);

        if (amountPresent) {
            final long amount = in.readLong();
            final String cur = in.readString();
            return new Amount(amount, cur);
        } else {
            return null;
        }
    }
}
