package com.kioli.photoshopping.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {
    public static final long              ONE_SECOND_MS    = 1000L;
    public static final long              ONE_MINUTE_MS    = 60L * ONE_SECOND_MS;
    public static final long              ONE_HOUR_MS      = 60L * ONE_MINUTE_MS;
    public static final long              ONE_DAY_MS       = 24L * ONE_HOUR_MS;

    private static final TimeZone         UTC_ZONE         = TimeZone.getTimeZone("UTC");
    private static final TimeZone         DUTCH_ZONE       = TimeZone.getTimeZone("Europe/Amsterdam");

    private final static SimpleDateFormat formatterDate    = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
    private final static SimpleDateFormat formatDateTime     = new SimpleDateFormat("dd-MM-yyyy_HH:mm", Locale.getDefault());

    static {
        /* Scheduled payments have 1 hour difference so using UTC renders them properly to the Amsterdam time. */
        formatterDate.setTimeZone(UTC_ZONE);
        formatDateTime.setTimeZone(DUTCH_ZONE);
    }

    public static String formatDate(final long timestamp) {
        return formatterDate.format(new Date(timestamp));
    }

    public static String formatDateTime(final long timestamp) {
        return formatDateTime.format(new Date(timestamp));
    }

    public static boolean isToday(final long timestamp) {
        final Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timestamp);
        final Calendar cal2 = Calendar.getInstance();
        cal2.setTimeInMillis(System.currentTimeMillis());
        return isSameDay(cal1, cal2);
    }

    public static boolean isSameDay(final Calendar cal1, final Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }

        final boolean result = cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA)
                        && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                        && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);

        return (result);
    }

    public static String getNow() {
        final Date d = new Date();
        final SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault());
        return sd.format(d);
    }

    /** Hide constructor for utility classes. */
    private DateUtils() {}
}
