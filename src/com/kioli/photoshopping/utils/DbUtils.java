package com.kioli.photoshopping.utils;

import java.io.File;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DbUtils {

    private static final Integer INT_FALSE = Integer.valueOf(0);
    private static final Integer INT_TRUE  = Integer.valueOf(1);

    public static Integer booleanToDb(final Boolean b) {
        return (b == Boolean.TRUE) ? INT_TRUE : INT_FALSE;
    }

    public static Boolean booleanFromDb(final Cursor cursor, final int column) {
        return (cursor.getInt(column) == INT_TRUE.intValue()) ? Boolean.TRUE : Boolean.FALSE;
    }


    public static <E extends Enum<E>> Integer enumToDb(final E value) {
        return Integer.valueOf(EnumUtils.toIntValue(value));
    }

    public static <E extends Enum<E>> E enumFromDb(final Cursor cursor, final int column, final Class<E> cls) {
        final int ord = cursor.getInt(column);
        return EnumUtils.fromIntValue(cls, ord);
    }


    public static String makeDropSql(final String tableName) {
        return "DROP TABLE IF EXISTS " + tableName;
    }

    public static String makeCreateSql(final String tableName, final String fields) {
        return "CREATE TABLE IF NOT EXISTS " + tableName + " (" + fields + ")";
    }

    public static String makeCreateIndexSql(final String tableName, final String indexName, final String fields) {
        return "CREATE INDEX IF NOT EXISTS " + indexName + " ON "  + tableName + " (" + fields + ")";
    }

    public static String makeDropIndexSql(final String tableName, final String indexName) {
        return "DROP INDEX IF  EXISTS " + indexName;
    }

    /** This is utility function for easily getting records count, e.g. for the WindowedAdapter. */
    public static int getRecordsCount(final SQLiteDatabase db, final String tableName) {
        final int cnt;
        Cursor counter = null;
        try {
            counter = db.rawQuery("SELECT COUNT(*) FROM " + tableName, null);
            counter.moveToFirst();
            cnt = counter.getInt(0);
        } finally {
            if (counter != null) counter.close();
        }
        return cnt;
    }

    /** This is utility function for easily getting records count, e.g. for the WindowedAdapter. */
    public static int getRecordsCountForSearchParams(final SQLiteDatabase db, final String tableName, final String colName, final String searchParams) {
        final int cnt;
        Cursor counter = null;
        try {
            counter = db.rawQuery("SELECT COUNT(*) FROM " + tableName + " WHERE " + colName + " LIKE ? ", new String[]{searchParams});
            counter.moveToFirst();
            cnt = counter.getInt(0);
        } finally {
            if (counter != null) counter.close();
        }
        return cnt;
    }

    public static void ensureWritableTable(final SQLiteDatabase db, final String tableName, final String fields, final boolean clean) {
        if (clean) {
            /* dropping and creating instead of "DELETE *" removes the overhead from updating indexes. */
            db.execSQL(DbUtils.makeDropSql(tableName));
        }

        final String createSql = DbUtils.makeCreateSql(tableName, fields);
        db.execSQL(createSql);

        /* "CREATE TABLE" sql sometimes fails internally without reporting any error, just quetly.
         * Check that table was indeed created and print logs if it's not.*/
        try {
            /* try doing something on the table, like counting rows */
            getRecordsCount(db, tableName);
        } catch (final Exception e) {
            Log.e("DBERROR", "Creating table " + tableName + " failed for some reason!\n" +
                  "SQL was: " + createSql);
        }
    }

    public static void ensureIndexOnTable(final SQLiteDatabase db, final String tableName, final String indexName,
                    final String fields) {
        final String indexSql = DbUtils.makeCreateIndexSql(tableName, indexName, fields);
        db.execSQL(indexSql);
    }

    /* Efren */
    public static boolean tableExistsInDatabase(final SQLiteDatabase db, final String tableName){
        try {
            getRecordsCount(db,tableName);
            return true;
        }
        catch (final Exception e )
        {
            // Something went wrong, so table does not exist
            return false;
         }
    }

    /**
     * Check if a database exists
     * @param context any context
     * @param dbName the database name to check
     * @return true if it exists, false otherwise
     */
    public static boolean doesDatabaseExist(final Context context, final String dbName) {
        final File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }

    /** Hide constructor for utility classes. */
    private DbUtils() { }
}
