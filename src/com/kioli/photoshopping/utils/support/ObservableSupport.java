package com.kioli.photoshopping.utils.support;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;

public class ObservableSupport<T> {
    public interface Delegate<T> {
        void execute(T obs);
    }

    private final HashSet<WeakReference<T>> observers = new HashSet<WeakReference<T>>(5);

    public synchronized void addObserver(final T observer) {
        final WeakReference<T> wrap = new WeakReference<T>(observer);
        observers.add(wrap);
    }

    public synchronized void removeObserver(final T observer) {
        int dead = 0;
        for (final WeakReference<T> wrap : observers) {
            final T obs = wrap.get();
            if (obs != null) {
                if (obs == observer) {
                    wrap.clear();
                    ++dead;
                    break;
                }
            } else {
                ++dead;
            }
        }

        if (dead > 3) {
            cleanupDead();
        }
    }

    public synchronized void notifyObservers(final Delegate<T> delegate) {
        int dead = 0;
        for (final WeakReference<T> wrap : observers) {
            final T obs = wrap.get();
            if (obs != null) {
                delegate.execute(obs);
            } else {
                ++dead;
            }
        }

        if (dead > 3) {
            cleanupDead();
        }
    }

    private void cleanupDead() {
        final Iterator<WeakReference<T>> iter = observers.iterator();
        while (iter.hasNext()) {
            final WeakReference<T> wrap = iter.next();
            if (wrap.get() == null) {
                iter.remove();
            }
        }
    }
}
