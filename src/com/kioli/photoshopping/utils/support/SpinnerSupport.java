package com.kioli.photoshopping.utils.support;

import android.os.Parcel;

import com.kioli.photoshopping.ui.collection.fragment.interfaces.ILoadingObserver;
import com.kioli.photoshopping.utils.ParcelableUtils;
import com.kioli.photoshopping.utils.support.ObservableSupport.Delegate;

public class SpinnerSupport implements ILoadingObserver {
    private final ObservableSupport<ILoadingObserver> observers = new ObservableSupport<ILoadingObserver>();
    private boolean isSpinning;

    public SpinnerSupport() {
        isSpinning = false;
    }

    public SpinnerSupport(final Parcel in) {
        isSpinning = ParcelableUtils.readBoolean(in).booleanValue();
    }

    public void writeState(final Parcel out, final int flags) {
    	ParcelableUtils.writeBoolean(out, Boolean.valueOf(isSpinning));
    }

    public boolean getIsSpinning() {
        return isSpinning;
    }

    @Override
    public synchronized void onLoadingStarted() {
        isSpinning = true;

        observers.notifyObservers(new Delegate<ILoadingObserver>() {
            @Override
            public void execute(final ILoadingObserver obs) {
                obs.onLoadingStarted();
            }
        });
    }

    @Override
    public synchronized void onLoadingFinished() {
        isSpinning = false;

        observers.notifyObservers(new Delegate<ILoadingObserver>() {
            @Override
            public void execute(final ILoadingObserver obs) {
                obs.onLoadingFinished();
            }
        });
    }

    public synchronized void addObserver(final ILoadingObserver observer) {
        observers.addObserver(observer);

        if (isSpinning) {
            observer.onLoadingStarted();
        } else {
            observer.onLoadingFinished();
        }
    }

    public boolean registerSpinnerObserverIfApplicable(final Object loadingObserver) {
        if (loadingObserver instanceof ILoadingObserver) {
            /* Let the fragment receive notifications */
            final ILoadingObserver obs = (ILoadingObserver) loadingObserver;
            addObserver(obs);
            return true;
        } else {
            return false;
        }
    }
}
