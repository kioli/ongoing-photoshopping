package com.kioli.photoshopping.utils;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kioli.photoshopping.PhotoShopApp;
import com.kioli.photoshopping.R;

public class AppRater {
	private static String	APP_TITLE;

	public static void showRateDialog(final Context mContext) {
		final int stringId = mContext.getApplicationInfo().labelRes;
		APP_TITLE = mContext.getString(stringId);

		final Dialog dialog = new Dialog(mContext);
		dialog.setTitle(mContext.getString(R.string.rate_the_app_title, APP_TITLE));

		final LinearLayout ll = new LinearLayout(mContext);
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setGravity(Gravity.CENTER);

		final TextView tv = new TextView(mContext);
		tv.setText(mContext.getString(R.string.rate_the_app_text, APP_TITLE));
		tv.setPadding(5, 5, 5, 5);
		ll.addView(tv);

		final Button b1 = new Button(mContext);
		b1.setText(mContext.getString(R.string.rate_the_app_btn_rate, APP_TITLE));
		b1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				try {
					mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + mContext.getPackageName())));
				} catch (final ActivityNotFoundException e) {
					Toast.makeText(mContext, R.string.rate_the_app_absent_browser, Toast.LENGTH_LONG).show();
				}
				PhotoShopApp.setAppRated();
				dialog.dismiss();
			}
		});
		ll.addView(b1);

		final Button b2 = new Button(mContext);
		b2.setText(mContext.getString(R.string.rate_the_app_btn_later));
		b2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				dialog.dismiss();
			}
		});
		ll.addView(b2);

		final Button b3 = new Button(mContext);
		b3.setText(mContext.getString(R.string.rate_the_app_btn_no));
		b3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				PhotoShopApp.setAppRated();
				dialog.dismiss();
			}
		});
		ll.addView(b3);

		dialog.setContentView(ll);
		dialog.show();
	}
}