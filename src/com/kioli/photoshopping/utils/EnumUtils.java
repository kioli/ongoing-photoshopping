package com.kioli.photoshopping.utils;



final public class EnumUtils {

    public static <E extends Enum<E>> int toIntValue(final E value) {
        if (value == null) {
            return -1;
        } else {
            return value.ordinal();
        }
    }

    public static <E extends Enum<E>> E fromIntValue(final Class<E> cls, final int ord) {
        if (ord == -1) {
            return null;
        } else {
            final E[] values = cls.getEnumConstants();
            return values[ord];
        }
    }

    public static <E extends Enum<E>> String[] toStringArray(final Class<E> cls) {
        return toStringArray(cls, null);
    }

    public static <E extends Enum<E>> String[] toStringArray(final Class<E> cls, final String fieldName) {
        final E[] values = cls.getEnumConstants();
        final int length = values.length;
        final String[] res = new String[length];

        for (int i = 0; i < length; i++) {
            final E s = values[i];
            res[i] = s.toString();
        }

        return res;
    }
    
    /** Hide constructor for utility classes. */
    private EnumUtils() { }
}
