package com.kioli.photoshopping.utils;

import android.content.Context;
import android.util.DisplayMetrics;

import com.kioli.photoshopping.PhotoShopApp;


public class ScreenUtils {

	/**
	 * Convert density independent pixels to regular pixels
	 * @param dip
	 * @return pixels
	 */
	public static int convertDipToPixels(final int dip){
		// Convert the dips to pixels
		final float scale = getScale();
		return(int) ((dip) * scale + 0.5f);
	}

	private static Float sScale;

    protected static float getScale() {
        if (sScale == null) {
            final Context context = PhotoShopApp.getContext();
            sScale = Float.valueOf(context.getResources().getDisplayMetrics().density);
        }
        return sScale.floatValue();
    }

    /**
     * Returns the screen orientations
     *
     * @return 0 - landscape, 1 - portrait, 2 - squared
     */
    public static int getScreenOrientation() {
    	final Context context = PhotoShopApp.getContext();
		final DisplayMetrics display = context.getResources().getDisplayMetrics();

        final int width = display.widthPixels;
        final int height = display.heightPixels;

		if(width > height)
			return 0;
		else if (width < height)
			return 1;
		else
			return 2;
    }

    /** Hide constructor for utility classes. */
    private ScreenUtils() {}
}
