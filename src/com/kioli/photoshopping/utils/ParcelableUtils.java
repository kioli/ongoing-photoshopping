package com.kioli.photoshopping.utils;

import java.util.EnumSet;
import java.util.Iterator;

import android.os.Parcel;

final public class ParcelableUtils {
    public static <E extends Enum<E>> void writeEnumSet(final Parcel out, final EnumSet<E> enums) {
        if (enums != null) {
            final int enumSize = enums.size();
            out.writeInt(enumSize);
            if (enumSize > 0) {
                final int[] ordinals = new int[enumSize];
                int arrayPosition = 0;
                final Iterator<E> iterator = enums.iterator();
                while (iterator.hasNext()) {
                    final Enum<E> item = iterator.next();
                    ordinals[arrayPosition] = item.ordinal();
                    arrayPosition++;
                }
                out.writeIntArray(ordinals);
            } else {
                /* no sense in writting an empty array */
            }
        } else {
            out.writeInt(-1);
        }
    }

    public static <E extends Enum<E>> EnumSet<E> readEnumSet(final Parcel in, final Class<E> cls) {
        final int enumSize = in.readInt();
        if (enumSize == -1) return null;

        final EnumSet<E> set = EnumSet.noneOf(cls);
        if (enumSize > 0) {
            final int[] indexArrayActionsValues = new int[enumSize];
            in.readIntArray(indexArrayActionsValues);
            final E[] values = cls.getEnumConstants();
            for (final int ordinal : indexArrayActionsValues) {
                set.add(values[ordinal]);
            }
        }

        return set;
    }

    public static <E extends Enum<E>> void writeEnum(final Parcel out, final E value) {
        final int ord = EnumUtils.toIntValue(value);
        out.writeInt(ord);
    }

    public static <E extends Enum<E>> E readEnum(final Parcel in, final Class<E> cls) {
        final int ord = in.readInt();
        return EnumUtils.fromIntValue(cls, ord);
    }

    public static Boolean readBoolean(final Parcel in){
        final int intValue = in.readInt();
        switch (intValue) {
        case -1:
            return null;
        case 1:
            return Boolean.TRUE;
        case 0:
            return Boolean.FALSE;
        default:
            throw new IllegalArgumentException("Invalid Boolean parse value");
        }
    }

    public static void writeBoolean(final Parcel out, final Boolean inputValue){
        int insertValue = -2; //Some invalid value
        if(inputValue==null){
            insertValue = -1;
        }else if(inputValue == Boolean.TRUE){
            insertValue = 1;
        }else if(inputValue == Boolean.FALSE){
            insertValue = 0;
        }
        out.writeInt(insertValue);
    }

    /** Hide constructor for utility classes. */
    private ParcelableUtils() { }
}
