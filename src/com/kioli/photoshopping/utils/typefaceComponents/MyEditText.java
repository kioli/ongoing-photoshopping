package com.kioli.photoshopping.utils.typefaceComponents;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.EditText;

import com.kioli.photoshopping.R;
import com.kioli.photoshopping.utils.FontUtils;

public class MyEditText extends EditText {

	private String	font;

	public MyEditText(final Context context) {
		this(context, null);
	}

	public MyEditText(final Context context, final AttributeSet attrs) {
		// Passes the custom attribute defined as a theme one (which in turns has a custom attribute 'font' relating to an asset font)
		this(context, attrs, R.attr.fontEditTextStyle);
	}

	public MyEditText(final Context context, final AttributeSet attrs, final int defStyle) {
		super(context, attrs, defStyle);

		// This method returns an array of the styleable attributes (from MyComponent in attrs.xml) present ion the XML of this component.
		// Since we provide a default style we don't need to write them in each XML but they will be read from the attribute specified in the theme (fontTextViewStyle)
		final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MyComponents, defStyle, 0);

		// Sets the String as what defined in the AppTheme attribute to be the value of MyComponents_font
		font = a.getString(R.styleable.MyComponents_font);
		a.recycle();

		setFont(font);
	}

	/**
	 * Set the font, by providing the string resource id indicating the font
	 * filename (located in /assets)
	 *
	 * @param fontResId
	 */
	public void setFont(final int fontResId) {
		setFont(getResources().getString(fontResId));
	}

	/**
	 * Set custom font, by providing the font filename (located in /assets)
	 *
	 * @param font
	 */
	public void setFont(final String font) {
		this.font = font;

		// http://stackoverflow.com/questions/10952894/custom-font-in-android-renders-differently-in-different-apis
		// using subpixel rendering to fix rendering issues in ICS
		setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);

		setTypeface(FontUtils.getTypeface(getContext(), font));
	}

	/**
	 * Get the current font filename, or null if not defined
	 *
	 * @return
	 */
	public String getFont() {
		return font;
	}
}
