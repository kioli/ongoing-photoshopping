package com.kioli.photoshopping.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class ActivityUtils {

	private ActivityUtils() {}
	
	/** <p>Show the virtual keyboard.
	 *  <p><i><b>NOTE:</b> WILL TRIGGER IT ONLY IF NO PHYSICAL KEYBOARD IS OPEN!</i>*/
	public static void showSoftKeyboard(final View v) {
		if (v == null) return;

		try {
			final Context context = v.getContext();
			final InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
		} catch (final UnsupportedOperationException uoe) {
			/* =======================================================================
			 * When showing a control in layout editor an UOE exception is provoked
			 * (because the editor doesn't have a keyboard).
			 * =======================================================================
			 * DO NOTHING.                                                          */
		}
	}

	public static void showSoftKeyboardWithDelay(final View v, final long delay) {
		if (v == null) return;

		v.postDelayed(new Runnable() {
			@Override
			public void run() {
				showSoftKeyboard(v);
			}
		}, delay);
	}

	/** Hide the virtual keyboard.
	 * @param v the currently focused input. Can be {@code null}. */
	public static void hideSoftKeyboard(final View v) {
		if (v == null) return;

		try {
			final Context context = v.getContext();
			final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		} catch (final UnsupportedOperationException uoe) {
			/* =======================================================================
			 * When showing a control in layout editor an UOE exception is provoked
			 * (because the editor doesn't have a keyboard).
			 * =======================================================================
			 * DO NOTHING.                                                          */
		}
	}
}
