package com.kioli.photoshopping.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.kioli.photoshopping.PhotoShopApp;

public class ImageUtils {

	private final static String imageExtension = ".jpg";

	public static File createImageFile() throws IOException {
		final File storageDir = PhotoShopApp.getStorageDirectory();
		final StringBuilder builder = new StringBuilder(DateUtils.formatDateTime((new Date()).getTime()));
		final File image = File.createTempFile(builder.toString(), imageExtension, storageDir);
		return image;
	}

	public static Bitmap resizeBitmapFromAbsolutePath(final String absolutePath, final int newWidth, final int newHeight) {
		final Bitmap bitmap = getBitmapFromFile(absolutePath);
		return Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
	}
	
	public static Bitmap getBitmapFromFile(final String absolutePath) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		return BitmapFactory.decodeFile(absolutePath, options);
	}

	public static boolean storeBitmapInFile(final Bitmap bitmap, final File file) {
		FileOutputStream out;
		try {
			final String filePath = file.getAbsolutePath();
			out = new FileOutputStream(filePath);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
			out.close();
		} catch (final Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Method to crop an image to make it square according to its lower
	 * dimension (optionally the user can also ask for the image to be shrunk
	 * even more by defining the new dimension
	 */
	public static Bitmap cropBitmap(final Bitmap origin, final int newDim) {
		Bitmap result = null;
		if (origin != null) {
			final int h = origin.getHeight();
			final int w = origin.getWidth();
			if (w >= h) {
				result = Bitmap.createBitmap(origin, w / 2 - h / 2, 0, h, h);
			} else {
				result = Bitmap.createBitmap(origin, 0, h / 2 - w / 2, w, w);
			}

			if (newDim != 0) {
				result = Bitmap.createScaledBitmap(result, newDim, newDim, true);
			}
		}

		return result;
	}
	
	/** Hide constructor for utility classes. */
	private ImageUtils() {
	}
}