package com.kioli.photoshopping;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.TypedArray;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.kioli.photoshopping.component.SettingsToggle.TRANSPORTATION;
import com.kioli.photoshopping.db.ObjectDbHelper;
import com.kioli.photoshopping.db.ObjectManager;
import com.kioli.photoshopping.gps.GpsTracker;
import com.kioli.photoshopping.model.ModelConvertHelper;
import com.kioli.photoshopping.model.ModelItemShopping;
import com.kioli.photoshopping.model.ModelProfile;
import com.kioli.photoshopping.model.ModelTypeObject;
import com.kioli.photoshopping.utils.AppRater;
import com.kioli.photoshopping.utils.amount.CURRENCY;

public class PhotoShopApp extends Application {
	private static final boolean DEBUG = false;
	private static Context context;
	private static GpsTracker tracker;
	private static File storageDir;

	public static final String	 TAG	                    = "photo shopping";
	private static final String	 SP_PROFILE	                = "user_profile";
	private static final String	 SP_LAUNCH_DATE	            = "launching_date";
	private static final String	 SP_LAUNCH_TIMES	        = "launching_times";
	private static final String	 SP_RATING_DONE	            = "rating_done";

	private final static int	 DAYS_UNTIL_RATE_PROMPT	    = 7;
	private final static int	 LAUNCHES_UNTIL_RATE_PROMPT	= 3;

	@Override
	public void onCreate() {
		super.onCreate();
		if (DEBUG) {
	        Log.d(TAG, "Application creating");
        }
		context = getApplicationContext();
		tracker = new GpsTracker(context);
		storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

		Crashlytics.start(this);
		getProfile();
//		resetProfile();
		
		photoCleaning();
	}

	public static Context getContext() {
		return context;
	}

	public static SharedPreferences getPreferences() {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}

	public static GpsTracker getTracker() {
		return tracker;
	}

	public static File getStorageDirectory() {
		return storageDir;
	}
	
	public static ModelProfile getProfile() {
	    String json =  getPreferences().getString(SP_PROFILE, "");

		if (json.length() == 0) {
			TypedArray colors = context.getResources().obtainTypedArray(R.array.default_types_colors);
			TypedArray types = context.getResources().obtainTypedArray(R.array.default_types);
			List<ModelTypeObject> list = new ArrayList<ModelTypeObject>();

			for (int i=0; i<types.length(); i++) {
				ModelTypeObject obj = new ModelTypeObject();
				obj.type_color=colors.getColor(i, 0);
				obj.type_name=types.getString(i);
				list.add(obj);
			}
			
			colors.recycle();
			types.recycle();
			
			final ModelProfile profile = ModelConvertHelper.createModelProfile(CURRENCY.EURO, null, TRANSPORTATION.DRIVING, list);
			final String stringProfile = setProfile(profile);
			json = json.concat(stringProfile);
		}

		return new Gson().fromJson(json, ModelProfile.class);
	}

	public static String setProfile(final ModelProfile profile) {
		final String json = new Gson().toJson(profile);
		final Editor prefsEditor = getPreferences().edit();
		prefsEditor.putString(SP_PROFILE, json);
		prefsEditor.commit();
		return json;
	}
	
	public static void resetProfile() {
		final Editor prefsEditor = getPreferences().edit();
		prefsEditor.putString(SP_PROFILE, "");
		prefsEditor.commit();
	}

	/**
	 * Method that runs every time the app is launched and cleans the directory
	 * from imag files that are not present in the DB anymore
	 */
	private void photoCleaning() {
		if (ObjectDbHelper.getHelper(this).getDatabase() != null) {
			final List<ModelItemShopping> listObjects = ObjectManager.getAllObjects();
			final List<File> deletableFiles = new ArrayList<File>();

			if (storageDir != null) {
				final File[] files = storageDir.listFiles();
				for (final File file : files) {
					boolean toDelete = true;
					for (final ModelItemShopping item : listObjects) {
						if (item.imgpath.equals(file.getAbsolutePath())) {
							toDelete = false;
						}
					}
					if (toDelete) {
						deletableFiles.add(file);
					}
				}

				for (final File file : deletableFiles) {
					file.delete();
				}
			}
		}
	}

	public static void checkRatingApp(final Context context) {
		final SharedPreferences prefs = getPreferences();
		if (prefs.getBoolean(SP_RATING_DONE, false)) {
			return;
		}

		final SharedPreferences.Editor editor = prefs.edit();

		// Increment launch counter
		final long launch_count = prefs.getLong(SP_LAUNCH_TIMES, 0) + 1;
		editor.putLong(SP_LAUNCH_TIMES, launch_count);

		// Get date of first launch
		Long date_firstLaunch = prefs.getLong(SP_LAUNCH_DATE, 0);
		if (date_firstLaunch == 0) {
			date_firstLaunch = System.currentTimeMillis();
			editor.putLong(SP_LAUNCH_DATE, date_firstLaunch);
		}

		// Wait at least n days before opening
		if (launch_count >= LAUNCHES_UNTIL_RATE_PROMPT) {
			if (System.currentTimeMillis() >= date_firstLaunch + (DAYS_UNTIL_RATE_PROMPT * 24 * 60 * 60 * 1000)) {
				AppRater.showRateDialog(context);
			}
		}

		editor.commit();
	}

	public static void setAppRated() {
		final Editor editor = getPreferences().edit();
		editor.putBoolean(SP_RATING_DONE, true);
		editor.commit();
	}
}
