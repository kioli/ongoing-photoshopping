package com.kioli.photoshopping.animation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;

public class GeneralAnimations {
	
	static public int FADE_OUT_DURATION = 500;
	
	public static void expand(View summary, int dimExpansion) {
	    summary.setVisibility(View.VISIBLE);
	    ValueAnimator mAnimator = slideAnimator(0, dimExpansion, summary);
	    mAnimator.start();
	}

	public static void collapse(final View summary, int dimCollapsing) {
		ValueAnimator mAnimator = slideAnimator(dimCollapsing, 0, summary);

	    mAnimator.addListener(new Animator.AnimatorListener() {
	        @Override
	        public void onAnimationEnd(Animator animator) {
	            summary.setVisibility(View.GONE);
	        }

	        @Override
	        public void onAnimationStart(Animator animator) {}

	        @Override
	        public void onAnimationCancel(Animator animator) {}

	        @Override
	        public void onAnimationRepeat(Animator animator) {}
	    });
	    mAnimator.start();
	}
	
	public static void fadeOut(final View viewFading, final View viewAppearing, final int duration) {
		ObjectAnimator fade_out = ObjectAnimator.ofFloat(viewFading, "alpha", 1, 0);
		fade_out.setDuration(duration);
        fade_out.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
            	viewFading.setVisibility(View.GONE);
            	viewAppearing.setVisibility(View.VISIBLE);
            }
        });
        fade_out.start();
	}

	private static ValueAnimator slideAnimator(int start, int end, final View summary) {
	    ValueAnimator animator = ValueAnimator.ofInt(start, end);
	    animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
	        @Override
	        public void onAnimationUpdate(ValueAnimator valueAnimator) {
	            //Update Height
	            int value = (Integer) valueAnimator.getAnimatedValue();
	            ViewGroup.LayoutParams layoutParams = summary.getLayoutParams();
	            layoutParams.height = value;
	            summary.setLayoutParams(layoutParams);
	        }
	    });
	    return animator;
	}
}
