package com.kioli.photoshopping.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.kioli.photoshopping.R;
import com.kioli.photoshopping.model.ModelItemShopping;
import com.kioli.photoshopping.ui.collection.fragment.phone.FragmentListPhone;
import com.kioli.photoshopping.utils.ImageUtils;

public class MyObjectsListAdapter extends BaseAdapter implements Filterable {
	private final Context context;
	private final FragmentListPhone frg;
	private final List<ModelItemShopping> list;
	private final List<ModelItemShopping> list_orig;

	static class ViewHolder {
		public TextView name;
		public TextView price;
		public TextView address;
		public ImageView image;
		public ImageView delete;
	}

	public MyObjectsListAdapter(final Context context, final FragmentListPhone frg, final List<ModelItemShopping> objects) {
		this.context = context;
		this.list = objects;
        this.list_orig = new ArrayList<ModelItemShopping>(objects);
        this.frg = frg;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		if (convertView == null) {
			final LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			convertView = inflater.inflate(R.layout.cmp_collection_list_row, null);
			final ViewHolder viewHolder = new ViewHolder();
			viewHolder.name = (TextView) convertView.findViewById(R.id.obj_row_name);
			viewHolder.price = (TextView) convertView.findViewById(R.id.obj_row_price);
			viewHolder.address = (TextView) convertView.findViewById(R.id.obj_row_address);
			viewHolder.image = (ImageView) convertView.findViewById(R.id.obj_row_img);
			viewHolder.delete = (ImageView) convertView.findViewById(R.id.obj_row_delete);
			convertView.setTag(viewHolder);
		}

		final ViewHolder holder = (ViewHolder) convertView.getTag();
		final ModelItemShopping item = list.get(position);
		holder.name.setText(item.name);
		holder.price.setText(item.price.toStringWithCurrency());
		holder.address.setText(list.get(position).address);
		holder.image.setImageBitmap(ImageUtils.getBitmapFromFile(list.get(position).imgpath));

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				frg.onItemSelected(position);
			}
		});
		
		holder.delete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				frg.onItemDeleted(position);
			}
		});

		return convertView;
	}

	@Override
	public int getCount() {
		if (list.size() == 0) {
			frg.onEmptyList(true);
		} else {
			frg.onEmptyList(false);
		}
		
		return list.size();
	}

	@Override
	public ModelItemShopping getItem(final int position) {
		if (position >= getCount() || position < 0) {
	        return null;
        }
		return list == null ? null : list.get(position);
	}

	@Override
	public long getItemId(final int position) {
		return position;
	}
	
	@Override
	public Filter getFilter() {
		return new Filter(){
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				constraint = constraint.toString().toLowerCase();
				final FilterResults result = new FilterResults();
				if (constraint != null && constraint.toString().length() > 0) {
					final List<ModelItemShopping> founded = new ArrayList<ModelItemShopping>();
					for (final ModelItemShopping item : list_orig) {
						if (item.name.contains(constraint)) {
							founded.add(item);
						}
					}
					result.values = founded;
					result.count = founded.size();
				} else {
					result.values = list_orig;
					result.count = list_orig.size();
				}
				return result;
			}
	        
			@Override
			protected void publishResults(final CharSequence constraint, final FilterResults results) {
				list.clear();
				final List<ModelItemShopping> values = (List<ModelItemShopping>) results.values;
				for (final ModelItemShopping item : values) {
					list.add(item);
				}
				notifyDataSetChanged();
			}
	    };
	}
}
