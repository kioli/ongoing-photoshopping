package com.kioli.photoshopping.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kioli.photoshopping.R;
import com.kioli.photoshopping.component.CoverFlow;
import com.kioli.photoshopping.model.ModelTypeObject;

public class MyCoverviewAdapter extends BaseAdapter {
	
	private List<ModelTypeObject> mObjects;
	private Context mContext;

	public MyCoverviewAdapter(Context context, List<ModelTypeObject>objects) {
		mContext = context;
		mObjects = objects;
	}

	public int getCount() {
		return mObjects.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

    private class ViewHolder {
        TextView text;
        TextView initial;
        LinearLayout layout;
    }
    
	public View getView(int position, View v, ViewGroup parent) {
		if (v == null) {
			v = LayoutInflater.from(mContext).inflate(R.layout.cmp_type_object, parent, false);
			int dim = (int) mContext.getResources().getDimension(R.dimen.object_type_carousel);
			v.setLayoutParams(new CoverFlow.LayoutParams(dim, dim));

            ViewHolder holder = new ViewHolder();
            holder.text = (TextView)v.findViewById(R.id.type_text);
            holder.initial = (TextView)v.findViewById(R.id.type_initial);
            holder.layout = (LinearLayout)v.findViewById(R.id.type_layout);
            v.setTag(holder);
        }

        ViewHolder holder = (ViewHolder)v.getTag();
        ModelTypeObject obj = mObjects.get(position);
        holder.text.setText(obj.type_name);
        holder.initial.setText(obj.type_name.charAt(0) + "");
        holder.layout.setBackgroundColor(obj.type_color);

        return v;
	}
}