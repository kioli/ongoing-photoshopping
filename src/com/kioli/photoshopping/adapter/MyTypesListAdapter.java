package com.kioli.photoshopping.adapter;

import java.util.List;

import android.content.ClipData.Item;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kioli.photoshopping.R;
import com.kioli.photoshopping.model.ModelTypeObject;
import com.kioli.photoshopping.ui.ActivityListTypes;

public class MyTypesListAdapter extends BaseAdapter {
	private final ActivityListTypes activity;
	private final List<ModelTypeObject> list;

	static class ViewHolder {
		public ImageView delete;
		public TextView name;
		public LinearLayout layout;
	}

	public MyTypesListAdapter(final ActivityListTypes activity,
			final List<ModelTypeObject> objects) {
		this.activity = activity;
		this.list = objects;
	}

	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		if (convertView == null) {
			final LayoutInflater inflater = activity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.cmp_type_object_list_row,
					null);
			final ViewHolder viewHolder = new ViewHolder();
			viewHolder.name = (TextView) convertView
					.findViewById(R.id.obj_type_txt);
			viewHolder.layout = (LinearLayout) convertView
					.findViewById(R.id.obj_type_layout);
			convertView.setTag(viewHolder);
		}

		final ViewHolder holder = (ViewHolder) convertView.getTag();
		final ModelTypeObject item = list.get(position);
		holder.name.setText(item.type_name);

		/*
		 * To use if in the xml the background is set to be a selector of
		 * different drawables based on clicked status
		 * 
		 * StateListDrawable layers = (StateListDrawable) holder.layout.getBackground(); 
		 * LayerDrawable drawable = (LayerDrawable) layers.getCurrent();
		 */
		LayerDrawable drawable = (LayerDrawable) holder.layout.getBackground();
		GradientDrawable shape = (GradientDrawable) (drawable.findDrawableByLayerId(R.id.list_shape));
		shape.setColor(item.type_color);

		return convertView;
	}

	@Override
	public int getCount() {
		if (list.size() == 0) {
			activity.onEmptyList(true);
		} else {
			activity.onEmptyList(false);
		}
		return list.size();
	}

	@Override
	public ModelTypeObject getItem(final int position) {
		if (position >= getCount() || position < 0) {
			return null;
		}
		return list == null ? null : list.get(position);
	}

	@Override
	public long getItemId(final int position) {
		return position;
	}
}
