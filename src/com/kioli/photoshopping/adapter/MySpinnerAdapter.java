package com.kioli.photoshopping.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kioli.photoshopping.R;

public class MySpinnerAdapter extends BaseAdapter {
	private final Context context;
	private final List<String> list;

	static class ViewHolder {
		public TextView currency;
	}

	public MySpinnerAdapter(final Context act, final List<String> objects) {
		this.context = act;
		this.list = objects;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		if (convertView == null) {
			final LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			convertView = inflater.inflate(R.layout.cmp_spinner_row, null);
			final ViewHolder viewHolder = new ViewHolder();
			viewHolder.currency = (TextView) convertView.findViewById(R.id.spinner_text);
			convertView.setTag(viewHolder);
		}

		final ViewHolder holder = (ViewHolder) convertView.getTag();
		holder.currency.setText(list.get(position));

		return convertView;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public String getItem(final int position) {
		if (position >= getCount() || position < 0) {
	        return null;
        }
		return list == null ? null : list.get(position);
	}

	@Override
	public long getItemId(final int position) {
		return position;
	}
}
