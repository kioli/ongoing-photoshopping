package com.kioli.photoshopping.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class ModelThreadListResult implements Parcelable {
    public Bitmap   bitmap;
    public int   	position;

    public ModelThreadListResult() { }
    
    private ModelThreadListResult(final Parcel in) {
        position 	= in.readInt();
        bitmap 	= (Bitmap) in.readValue(Bitmap.class.getClassLoader());
    }

    /* ************************************  PARCELABLE  ******************************* */
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
    	dest.writeInt(position);
        dest.writeValue(bitmap);
    }
    
    public static final Parcelable.Creator<ModelThreadListResult> CREATOR = new Creator<ModelThreadListResult>() {
        @Override
        public ModelThreadListResult[] newArray(final int size) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ModelThreadListResult createFromParcel(final Parcel source) {
            return new ModelThreadListResult(source);
        }
    };
    /* ************************************ /PARCELABLE  ******************************* */
}
