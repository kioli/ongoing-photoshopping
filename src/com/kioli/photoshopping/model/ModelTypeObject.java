package com.kioli.photoshopping.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Singleton defining the user profile with its preferences
 */
public class ModelTypeObject implements Parcelable {

	public String type_name;
	public int type_color;

	public ModelTypeObject() { }
	
    protected ModelTypeObject(Parcel in) {
    	 type_name = in.readString();
         type_color = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }
    
    @Override
    public void writeToParcel(Parcel dest, int flags) {
    	dest.writeString(type_name);
        dest.writeInt(type_color);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ModelTypeObject> CREATOR = new Parcelable.Creator<ModelTypeObject>() {
        @Override
        public ModelTypeObject createFromParcel(Parcel in) {
            return new ModelTypeObject(in);
        }

        @Override
        public ModelTypeObject[] newArray(int size) {
            return new ModelTypeObject[size];
        }
    };
}
