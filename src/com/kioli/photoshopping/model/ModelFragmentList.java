package com.kioli.photoshopping.model;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelFragmentList implements Parcelable {
    public List<ModelItemShopping>   listObjects;

    public ModelFragmentList() { }

    private ModelFragmentList(final Parcel in) {
        in.readList(listObjects, ModelItemShopping.class.getClassLoader());
    }
    
    /* ************************************  PARCELABLE  ******************************* */
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel out, final int flags) {
        out.writeList(listObjects);
    }
    
    public static final Parcelable.Creator<ModelFragmentList> CREATOR = new Creator<ModelFragmentList>() {
        @Override
        public ModelFragmentList[] newArray(final int size) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ModelFragmentList createFromParcel(final Parcel source) {
            return new ModelFragmentList(source);
        }
    };
    /* ************************************ /PARCELABLE  ******************************* */
}
