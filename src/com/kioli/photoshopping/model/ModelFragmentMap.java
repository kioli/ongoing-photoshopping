package com.kioli.photoshopping.model;

import java.util.Hashtable;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class ModelFragmentMap implements Parcelable {

	public Hashtable<MarkerOptions, String> markers;

    public ModelFragmentMap() { }

    private ModelFragmentMap(final Parcel in) {
        markers = (Hashtable) in.readValue(Hashtable.class.getClassLoader());
    }
    
    /* ************************************  PARCELABLE  ******************************* */
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel out, final int flags) {
        out.writeValue(markers);
    }
    
    public static final Parcelable.Creator<ModelFragmentMap> CREATOR = new Creator<ModelFragmentMap>() {
        @Override
        public ModelFragmentMap[] newArray(final int size) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ModelFragmentMap createFromParcel(final Parcel source) {
            return new ModelFragmentMap(source);
        }
    };
    /* ************************************ /PARCELABLE  ******************************* */
}
