package com.kioli.photoshopping.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.kioli.photoshopping.component.SettingsToggle.TRANSPORTATION;
import com.kioli.photoshopping.utils.amount.Amount;

public class ModelFragmentDetails implements Parcelable {

	public int id;
	public String name;
	public Amount price;
	public double latitude;
	public double longitude;
	public String date;
	public String address;
	public Bitmap photo;
	public TRANSPORTATION transportation;
	
	public ModelFragmentDetails () {}

    private ModelFragmentDetails(Parcel in) {
        id = in.readInt();
        name = in.readString();
        price = Amount.readFromParcel(in);
        latitude = in.readDouble();
        longitude = in.readDouble();
        date = in.readString();
        address = in.readString();
        photo = (Bitmap) in.readValue(Bitmap.class.getClassLoader());
        transportation = (TRANSPORTATION) in.readValue(TRANSPORTATION.class.getClassLoader());
    }

    /* ************************************  PARCELABLE  ******************************* */
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        Amount.writeToParcel(dest, price);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(date);
        dest.writeString(address);
        dest.writeValue(photo);
        dest.writeValue(transportation);
    }

    public static final Parcelable.Creator<ModelFragmentDetails> CREATOR = new Parcelable.Creator<ModelFragmentDetails>() {
        @Override
        public ModelFragmentDetails createFromParcel(Parcel in) {
            return new ModelFragmentDetails(in);
        }

        @Override
        public ModelFragmentDetails[] newArray(int size) {
            return new ModelFragmentDetails[size];
        }
    };
    /* ************************************  PARCELABLE  ******************************* */
}