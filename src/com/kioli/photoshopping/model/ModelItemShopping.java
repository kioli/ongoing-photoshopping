package com.kioli.photoshopping.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.kioli.photoshopping.utils.amount.Amount;

public class ModelItemShopping implements Parcelable {

	public long  	id;
	public String 	name;
	public Amount 	price;
	public String 	currency;
	public double 	latitude;
	public double 	longitude;
	public String 	address;
	public String 	date;
	public String 	imgpath;
	public Bitmap	photo;
	public Bitmap	photoThumbnail;
//    public byte[] imageData;

	public ModelItemShopping () {}

    private ModelItemShopping(final Parcel in) {
        id = in.readLong();
        name = in.readString();
        price = Amount.readFromParcel(in);
        currency = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        address = in.readString();
        date = in.readString();
        imgpath = in.readString();
		photo = (Bitmap) in.readValue(Bitmap.class.getClassLoader());
		photoThumbnail = (Bitmap) in.readValue(Bitmap.class.getClassLoader());
//        imageData = new byte[in.readInt()];
//        in.readByteArray(imageData);
    }

    /* ************************************  PARCELABLE  ******************************* */
    @Override
    public int describeContents() {
        return 0;
    }

//    public void setImageData(final byte[] imageData) {
//        this.imageData = imageData;
//        this.photo     = null;
//    }

//    public Bitmap getImage() {
//        if (imageData == null) return null;
//
//        if (photo == null) {
//        	photo = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
//        }
//        return photo;
//    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        Amount.writeToParcel(dest, price);
        dest.writeString(currency);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(address);
        dest.writeString(date);
        dest.writeString(imgpath);
		dest.writeValue(photo);
		dest.writeValue(photoThumbnail);
//        dest.writeInt(imageData.length);
//        dest.writeByteArray(imageData);
    }

    public static final Parcelable.Creator<ModelItemShopping> CREATOR = new Parcelable.Creator<ModelItemShopping>() {
        @Override
        public ModelItemShopping createFromParcel(final Parcel in) {
            return new ModelItemShopping(in);
        }

        @Override
        public ModelItemShopping[] newArray(final int size) {
            return new ModelItemShopping[size];
        }
    };
    /* ************************************  PARCELABLE  ******************************* */
}