package com.kioli.photoshopping.model;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.kioli.photoshopping.component.SettingsToggle.TRANSPORTATION;
import com.kioli.photoshopping.utils.amount.CURRENCY;

/**
 * Singleton defining the user profile with its preferences
 */
public class ModelProfile implements Parcelable {

	public CURRENCY defaultCurrency;
	public LatLng homeLocation;
	public TRANSPORTATION transportation;
	public boolean address_street = true;
	public boolean address_postcode = false;
	public boolean address_country = true;
	public List<ModelTypeObject> object_types;

	protected ModelProfile() { }
	
    private ModelProfile(Parcel in) {
        defaultCurrency = (CURRENCY) in.readValue(CURRENCY.class.getClassLoader());
        homeLocation = (LatLng) in.readValue(LatLng.class.getClassLoader());
        transportation = (TRANSPORTATION) in.readSerializable();
        address_street = in.readByte() != 0x00;
        address_postcode = in.readByte() != 0x00;
        address_country = in.readByte() != 0x00;
        if (in.readByte() == 0x01) {
            object_types = new ArrayList<ModelTypeObject>();
            in.readList(object_types, ModelTypeObject.class.getClassLoader());
        } else {
            object_types = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }
    
    public TRANSPORTATION getTransportation() {
    	return transportation;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(defaultCurrency);
        dest.writeValue(homeLocation);
        dest.writeValue(transportation);
        dest.writeByte((byte) (address_street ? 0x01 : 0x00));
        dest.writeByte((byte) (address_postcode ? 0x01 : 0x00));
        dest.writeByte((byte) (address_country ? 0x01 : 0x00));
        if (object_types == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(object_types);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ModelProfile> CREATOR = new Parcelable.Creator<ModelProfile>() {
        @Override
        public ModelProfile createFromParcel(Parcel in) {
            return new ModelProfile(in);
        }

        @Override
        public ModelProfile[] newArray(int size) {
            return new ModelProfile[size];
        }
    };
}
