package com.kioli.photoshopping.model;

import java.util.List;

import com.google.android.gms.maps.model.LatLng;
import com.kioli.photoshopping.component.SettingsToggle.TRANSPORTATION;
import com.kioli.photoshopping.utils.amount.CURRENCY;

public class ModelConvertHelper {

    public static ModelProfile createModelProfile(final CURRENCY currency, final LatLng location, final TRANSPORTATION transport, final List<ModelTypeObject> types) {
        final ModelProfile model = new ModelProfile();

        model.defaultCurrency = currency;
        model.homeLocation = location;
        model.transportation = transport;
        model.address_country = true;
        model.address_postcode = false;
        model.address_street = true;
        model.object_types = types;

        return model;
    }
}
