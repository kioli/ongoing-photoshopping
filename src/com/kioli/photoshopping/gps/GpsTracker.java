package com.kioli.photoshopping.gps;

import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;

import com.kioli.photoshopping.PhotoShopApp;
import com.kioli.photoshopping.R;
import com.kioli.photoshopping.model.ModelProfile;

public class GpsTracker extends Service implements LocationListener {

	private Context mContext;

	private boolean isGPSEnabled = false;
	private boolean isNetworkEnabled = false;
	private boolean canGetLocation = false;

	private Location location;
	private static double latitude;
	private static double longitude;

	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 5;
	private static final long MIN_TIME_BW_UPDATES = 1000 * 2 * 1;

	// Declaring a Location Manager
	protected LocationManager locationManager;
	
	public GpsTracker(Context context) {
		this.mContext = context;
		checkWorking();
	}

	private void checkWorking() {
		locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
		isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		if (isGPSEnabled || isNetworkEnabled) {
			canGetLocation = true;
		}
	}
	
	public Location getLocation() {
		try {
			if (canGetLocation) {
				// First get location from Network Provider
				if (isNetworkEnabled) {
					locationManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
					if (locationManager != null) {
						location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					}
				} else {
					locationManager.requestLocationUpdates(
							LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
					if (locationManager != null) {
						location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
					}
				}
				
				if (location != null) {
					latitude = location.getLatitude();
					longitude = location.getLongitude();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return location;
	}

	public void stopUsingGPS() {
		if (locationManager != null) {
			locationManager.removeUpdates(GpsTracker.this);
		}
	}

	public double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();
		}

		return latitude;
	}

	public double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();
		}

		return longitude;
	}

	/**
	 * Function to check GPS/wifi enabled
	 * 
	 * @return boolean
	 * */
	public boolean canGetLocation() {
		return this.canGetLocation;
	}
	
	/**
	 * Returns a String with the address pointed by the two coordinates
	 * longitude and latitude
	 * 
	 * @param ctx
	 *            - ApplicationContext
	 * @return a String with the address formatted in
	 *         "NAME, LOCALITY, AREA, COUNTRY" or empty string
	 */
	public static String getAddressFromLocation(Context ctx) {
		try {
			Geocoder geo = new Geocoder(ctx, Locale.getDefault());
			StringBuilder builder = new StringBuilder();
			List<Address> addresses = geo.getFromLocation(latitude, longitude, 1);
			if (addresses.isEmpty()) {
				return "";
			} else {
				if (addresses.size() > 0) {
					ModelProfile profile = PhotoShopApp.getProfile();
					if (profile.address_street) {
						if (addresses.get(0).getThoroughfare() != null)
							builder.append(addresses.get(0).getThoroughfare() + " ");
						if (addresses.get(0).getSubThoroughfare() != null)
							builder.append(addresses.get(0).getSubThoroughfare() + " ");
					}
					if (profile.address_postcode) {
						if (addresses.get(0).getPostalCode() != null)
							builder.append(addresses.get(0).getPostalCode() + " ");
					}
					if (profile.address_country) {
						if (addresses.get(0).getLocality() != null)
							builder.append(addresses.get(0).getLocality());
					}
					return builder.toString();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * Function to show settings alert dialog On pressing Settings button will
	 * lauch Settings Options
	 * */
	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
		alertDialog.setTitle(getString(R.string.gps_error_title));
		alertDialog.setMessage(getString(R.string.gps_error_text));
		alertDialog.setPositiveButton(getString(R.string.gps_dialog_positive_btn),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						mContext.startActivity(intent);
					}
				});
		alertDialog.setNegativeButton(getString(R.string.gps_dialog_negative_btn),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		alertDialog.show();
	}

	@Override
	public void onLocationChanged(Location location) {
	}

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	  public int onStartCommand(Intent intent, int flags, int startId) {
	    return Service.START_NOT_STICKY;
	  }
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
}