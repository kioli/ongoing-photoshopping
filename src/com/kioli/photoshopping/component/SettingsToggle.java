package com.kioli.photoshopping.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.kioli.photoshopping.R;

public class SettingsToggle extends LinearLayout {

	public enum TRANSPORTATION {
		WALKING("walking"), DRIVING("driving");

		private String transport;

		private TRANSPORTATION(final String transport) {
			this.transport = transport;
		}

		public String getValue() {
			return transport;
		}
	}

	public interface IToggleCallback {
		void onToggleCLicked(TRANSPORTATION transport);
	}

	private final Button transportWalking;
	private final Button transportAuto;
	private IToggleCallback callback;

	public SettingsToggle(final Context context) {
		this(context, null);
	}

	public SettingsToggle(final Context context, final AttributeSet attrs) {
		super(context, attrs);

		final LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.cmp_settings_toggle, this);

		transportWalking = (Button) findViewById(R.id.toggle_walking);
		transportAuto = (Button) findViewById(R.id.toggle_auto);

		setListeners();
	}

	private void setListeners() {
		transportWalking.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				walkingSelected();
				callback.onToggleCLicked(TRANSPORTATION.WALKING);
			}
		});
		transportAuto.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				drivingSelected();
				callback.onToggleCLicked(TRANSPORTATION.DRIVING);
			}
		});
	}

	public void walkingSelected() {
		transportWalking.setBackgroundResource(R.drawable.squared_button_on);
		transportAuto.setBackgroundResource(R.drawable.squared_button_off);
	}

	public void drivingSelected() {
		transportWalking.setBackgroundResource(R.drawable.squared_button_off);
		transportAuto.setBackgroundResource(R.drawable.squared_button_on);
	}

	public void setCallback(IToggleCallback clb) {
		callback = clb;
	}

}
