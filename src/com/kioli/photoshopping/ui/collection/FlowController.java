package com.kioli.photoshopping.ui.collection;

import java.util.Hashtable;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kioli.photoshopping.PhotoShopApp;
import com.kioli.photoshopping.R;
import com.kioli.photoshopping.db.ObjectManager;
import com.kioli.photoshopping.model.ModelFragmentDetails;
import com.kioli.photoshopping.model.ModelFragmentList;
import com.kioli.photoshopping.model.ModelFragmentMap;
import com.kioli.photoshopping.model.ModelItemShopping;
import com.kioli.photoshopping.ui.collection.fragment.interfaces.IFlow;
import com.kioli.photoshopping.ui.collection.fragment.interfaces.IUiBridge;
import com.kioli.photoshopping.utils.ImageUtils;
import com.kioli.photoshopping.utils.ParcelableUtils;
import com.kioli.photoshopping.utils.amount.AmountUtils;

public class FlowController {

    public static final String PARCEL_ID = FlowController.class.getName();

    private static final class State implements Parcelable {
        public List<ModelItemShopping>       itemsList;
        public FLOW_STEP                    currentStep;

        public State() {
            this.currentStep = FLOW_STEP.IDLE;
        }

        private State(final Parcel in) {
            currentStep   = ParcelableUtils.readEnum(in, FLOW_STEP.class);
            itemsList = in.readParcelable(ModelItemShopping.class.getClassLoader());
        }

        @Override
        public void writeToParcel(final Parcel out, final int flags) {
            ParcelableUtils.writeEnum(out, currentStep);
            out.writeList(itemsList);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @SuppressWarnings("unused") // used by android
        public static final Creator<State> CREATOR = new Creator<State>() {
                   @Override
                   public State[] newArray(final int size) {
                       return null;
                   }

                   @Override
                   public State createFromParcel(final Parcel in) {
                       return new State(in);
                   }
               };
    }

    private final Context                       mContext;
    private final IFlow                    		mFlow;
    private IUiBridge                      		mUiBridge;

    private State                               mState;

    public FlowController(final FragmentActivity activity, final IFlow flow) {
        // We intentionally store only the context to prevent people making shortcut calls to the activity.
        // Using the mFlow instead of activity is advised.
        mContext  = activity;
        mFlow = flow;
    }

    public void setUiBridge(final IUiBridge bridge) {
        mUiBridge = bridge;
        mUiBridge.setFlowDelegate(mFlow);
    }

    /* ==================== LIFECYCLE HANDLING ========================= */
    public void restoreLastCustomNonConfigurationInstance(final FlowController last) {
        if (last == null) {
            this.mState = new State();
        } else {
            this.mState = last.mState;
        }
    }

    public void restoreSavedInstanceState(final Bundle savedInstanceState) {
        mState = savedInstanceState.getParcelable(PARCEL_ID);
        recoverLoadingState();
    }

    public void onSaveInstanceState(final Bundle outState) {
        outState.putParcelable(PARCEL_ID, mState);
    }

    /**
     * When the activity is destroyed, while a loader wasn't finished, we can't be aware of the result,
     * so we need to go back to the state before firing the call (and hide loading indication)
     */
    private void recoverLoadingState() {
        switch(mState.currentStep) {
        case LIST:
            mState.currentStep = FLOW_STEP.LIST;
            onLoadingFinished();
            break;
        case DETAILS:
            mState.currentStep = FLOW_STEP.DETAILS;
            onLoadingFinished();
            break;
        case IDLE:
        default:
            /* Do nothing */
            break;
        }
    }

    /* =============================================================== */
    private void onLoadingStarted() {
        mUiBridge.onLoadingStarted();
    }

    private void onLoadingFinished() {
        mUiBridge.onLoadingFinished();
    }

    public void goBack() {
        switch (mState.currentStep) {
        case IDLE:
            /* do nothing */
            break;
        case LIST:
        	mUiBridge.goBack(null);
            mState.currentStep = FLOW_STEP.IDLE;
            break;
        case MAP:
            mUiBridge.goBack(getFragmentListModel());
            mState.currentStep = FLOW_STEP.LIST;
            break;
        case DETAILS:
            mUiBridge.goBack(getFragmentListModel());
            mState.currentStep = FLOW_STEP.LIST;
            break;
        default:
            throw new UnsupportedOperationException("Not yet implemented for step " + mState.currentStep);
        }
    }

    /* ==================== STARTING THE CONTROLLER ========================= */
    public boolean isStarted() {
        return (mState.currentStep != FLOW_STEP.IDLE);
    }

    public void start(List<ModelItemShopping> list) {
		mState.itemsList = list;
		mState.currentStep = FLOW_STEP.LIST;
		mUiBridge.goTo(getFragmentListModel(), mState.currentStep);
    }

    /* ==================== STEP LIST ========================= */

    public void onItemSelected(int position) {
    	mState.currentStep = FLOW_STEP.DETAILS;
		mUiBridge.goTo(getFragmentDetailsModel(position), mState.currentStep);
	}
    
    public void onItemDeleted(int position) {
    	if (mState.currentStep == FLOW_STEP.LIST || mState.currentStep == FLOW_STEP.DETAILS) {
    		ObjectManager.removeObject(mState.itemsList.get(position));
    		mState.itemsList.remove(position);
    	}
    }
    
	public void onMapSelected() {
		mState.currentStep = FLOW_STEP.MAP;
		mUiBridge.goTo(getFragmentMapModel(), mState.currentStep);
	}
    
    /* ==================== MODEL CREATION ========================= */
    
    private ModelFragmentList getFragmentListModel() {
    	ModelFragmentList modelList = new ModelFragmentList();
    	modelList.listObjects = mState.itemsList;
		return modelList;
    }
    
    private ModelFragmentDetails getFragmentDetailsModel(int position) {
    	ModelFragmentDetails modelDetails = new ModelFragmentDetails();
    	ModelItemShopping item = mState.itemsList.get(position);
    	modelDetails.id = position;
    	modelDetails.date = item.date;
    	modelDetails.latitude = item.latitude;
    	modelDetails.longitude = item.longitude;
    	modelDetails.name = item.name;
    	modelDetails.address = item.address;
    	modelDetails.price = item.price;
    	modelDetails.transportation = PhotoShopApp.getProfile().transportation;
    	
    	int width = (int) mContext.getResources().getDimension(R.dimen.details_item_photo_dimension);
    	int height = (int) mContext.getResources().getDimension(R.dimen.details_item_photo_dimension);
    	modelDetails.photo = ImageUtils.resizeBitmapFromAbsolutePath(item.imgpath, width, height);
		return modelDetails;
    }
    
    private ModelFragmentMap getFragmentMapModel() {
    	ModelFragmentMap modelMap = new ModelFragmentMap();
    	
		final Hashtable<MarkerOptions, String> markers = new Hashtable<MarkerOptions, String>();
		for (ModelItemShopping item : mState.itemsList) {
			LatLng point = new LatLng(item.latitude, item.longitude);
			MarkerOptions marker = new MarkerOptions()
					.position(point)
					.title(item.name + " - " + AmountUtils.toStringWithCurrency(item.price))
					.snippet(item.address);
			markers.put(marker, item.imgpath);
		}
		modelMap.markers = markers;
		return modelMap;
    }
}