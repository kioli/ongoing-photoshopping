package com.kioli.photoshopping.ui.collection.fragment;

import android.support.v4.app.Fragment;

import com.kioli.photoshopping.ui.collection.FlowController;
import com.kioli.photoshopping.ui.collection.fragment.interfaces.IFlow;
import com.kioli.photoshopping.ui.collection.fragment.interfaces.IUiBridge;

abstract public class UiBridgeFragment extends Fragment implements IUiBridge {
    public static final String FRAGMENT_TAG = UiBridgeFragment.class.getName();

    protected FlowController mController;
    protected IFlow          mFlow;

    @Override
    public void setController(final FlowController controller) {
        mController = controller;
    }

    @Override
    public void setFlowDelegate(final IFlow flowDelegate) {
        this.mFlow = flowDelegate;
    }

}
