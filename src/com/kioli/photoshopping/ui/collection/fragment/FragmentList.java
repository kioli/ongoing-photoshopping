package com.kioli.photoshopping.ui.collection.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.kioli.photoshopping.ui.collection.ActivityCollection;
import com.kioli.photoshopping.ui.collection.fragment.interfaces.IFlow;
import com.kioli.photoshopping.ui.collection.fragment.interfaces.IUiBridge;

abstract public class FragmentList extends Fragment {
    public interface IBridge {
    	void onItemDeleted(int position);
        void onItemSelected(int position);
		void onMapSelected();
    }
    
    protected IBridge 		mBridge;
    protected IFlow       	mFlow;

    public void setController(final IBridge bridge) {
        this.mBridge = bridge;
    }

    public void setFlowDelegate(final IFlow mFlow) {
        this.mFlow = mFlow;
    }

    /**
     * checks if the activity holding the fragment implements its interface and automatically sets the object to allow
     * the fragment to communicate with the activity
     */
    @Override
    public void onAttach(final Activity activity) {
        final IUiBridge dir = ((ActivityCollection) activity).getmUiBridge();

        // Dir != null, since when Android destroys our activity, when resuming the Fragments the Director is still NOT
        // present
        if (dir != null) {
            if ((dir instanceof IBridge) == false) {
	            throw new ClassCastException("activity must implement IController");
            }
            super.onAttach(activity);
            mBridge = (IBridge) dir;
        } else {
            mBridge = null;
            super.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        mBridge = null;
        super.onDetach();
    }
}
