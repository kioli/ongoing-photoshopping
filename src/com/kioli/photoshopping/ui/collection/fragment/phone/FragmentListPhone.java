package com.kioli.photoshopping.ui.collection.fragment.phone;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.kioli.photoshopping.R;
import com.kioli.photoshopping.adapter.MyObjectsListAdapter;
import com.kioli.photoshopping.model.ModelFragmentList;
import com.kioli.photoshopping.ui.collection.fragment.FragmentList;
import com.kioli.photoshopping.ui.collection.fragment.interfaces.ILoadingObserver;
import com.kioli.photoshopping.utils.ActivityUtils;

public class FragmentListPhone extends FragmentList implements ILoadingObserver {

	private static final class Controls {
		ListView 		list;
		EditText 		search_collection;
		Button 			btn_map;
		LinearLayout 	spinner;
	}

    private static final String MODEL_ARGS_KEY = "model_list";

    private Controls        		controls;
    private boolean         		showSpinner = false;
    private ModelFragmentList     	mData;
    private MyObjectsListAdapter	adapter;
    
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        }
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    }

    private void restoreInstanceState(final Bundle state) {
        mData = state.getParcelable(MODEL_ARGS_KEY);
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        outState.putParcelable(MODEL_ARGS_KEY, mData);
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.frg_collection_list, container, false);

        controls = new Controls();
        
        initViews(view);
    	updateSpinner();
    	
        if(mData != null) {
            populate();
        }

        return view;
    }
    
	private void initViews(View view) {
		controls.list = (ListView) view.findViewById(R.id.list_objects);
		controls.spinner = (LinearLayout) view.findViewById(R.id.spinner);
		controls.search_collection = (EditText) view.findViewById(R.id.search_collection);
		controls.search_collection.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				adapter.getFilter().filter(s);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {}
		});

		controls.btn_map = (Button) view.findViewById(R.id.collection_to_map_btn);
		controls.btn_map.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mBridge.onMapSelected();
			}
		});
	}

    @Override
    public void onLoadingStarted() {
        showSpinner = true;
        updateSpinner();
    }

    @Override
    public void onLoadingFinished() {
        showSpinner = false;
        updateSpinner();
    }

    private void showSpinner(final boolean toShow) {
    	if (toShow) {
    		controls.spinner.setVisibility(View.VISIBLE);
    	} else {
    		controls.spinner.setVisibility(View.GONE);
    	}
    }
    
    public void setData(final ModelFragmentList model) {
        mData = model;
        if(controls != null) {
            populate();
        }
    }
    
    private void updateSpinner() {
        if(controls != null) {
            showSpinner(showSpinner);
        }
    }

    private void populate() {
        if (mData.listObjects != null) {
        	adapter = new MyObjectsListAdapter(getActivity(), this, mData.listObjects);
    		controls.list.setAdapter(adapter);
        }
    }
	
	public void onItemSelected(int position) {
		ActivityUtils.hideSoftKeyboard(controls.search_collection);
		mBridge.onItemSelected(position);
	}
	
	public void onItemDeleted(int position) {
		mBridge.onItemDeleted(position);
		adapter.notifyDataSetChanged();
	}
	
	public void onEmptyList(final boolean isListEmpty) {
		if (isListEmpty) {
			controls.list.setVisibility(View.INVISIBLE);
		} else {
			controls.list.setVisibility(View.VISIBLE);
		}
	}
}