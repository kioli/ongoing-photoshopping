package com.kioli.photoshopping.ui.collection.fragment.phone;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map.Entry;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kioli.photoshopping.PhotoShopApp;
import com.kioli.photoshopping.R;
import com.kioli.photoshopping.gps.GpsTracker;
import com.kioli.photoshopping.model.ModelFragmentMap;
import com.kioli.photoshopping.ui.collection.fragment.FragmentMap;
import com.kioli.photoshopping.ui.collection.fragment.interfaces.ILoadingObserver;
import com.kioli.photoshopping.utils.ImageUtils;

public class FragmentMapPhone extends FragmentMap implements ILoadingObserver {

	private static final class Controls {
		GoogleMap map;
	}

	private static final String MODEL_ARGS_KEY = "model_list";

	private final int 				zoomLevel = 14;
    private SupportMapFragment 	fragment;
	private Controls 			controls;
	private ModelFragmentMap 	mData;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			restoreInstanceState(savedInstanceState);
		}
	}
	
    @Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		createMap();
	}
    
    @Override
	public void onResume() {
		super.onResume();
		Log.e(PhotoShopApp.TAG, "RESUME");
		if (controls.map == null) {
			controls.map = fragment.getMap();
	        centerMap();
	        populateMap();
		}
	}

	private void restoreInstanceState(final Bundle state) {
		mData = state.getParcelable(MODEL_ARGS_KEY);
	}

	@Override
	public void onSaveInstanceState(final Bundle outState) {
		outState.putParcelable(MODEL_ARGS_KEY, mData);
		super.onSaveInstanceState(outState);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.frg_map, container, false);
	    
		controls = new Controls();

		return view;
	}

	public void setData(final ModelFragmentMap model) {
		Log.e(PhotoShopApp.TAG, "SET DATA");
		mData = model;
	}
	
    private void createMap() {
    	final FragmentManager fm = getChildFragmentManager();
		fragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
		if (fragment == null) {
			fragment = SupportMapFragment.newInstance();
			fm.beginTransaction().replace(R.id.map_container, fragment).commit();
		}
    }
    
    private void centerMap() {
		final GpsTracker tracker = PhotoShopApp.getTracker();
		final Location myLocation = tracker.getLocation();
		tracker.stopUsingGPS();
		final LatLng point = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());

    	final LatLng pointHome = PhotoShopApp.getProfile().homeLocation;
    	if (pointHome != null && point != null) {
            // Add marker house
            final MarkerOptions marker_home = new MarkerOptions().position(pointHome);
            marker_home.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_home));
            controls.map.addMarker(marker_home);
        }
    	
        controls.map.moveCamera(CameraUpdateFactory.newLatLngZoom(point, zoomLevel));
        controls.map.setMyLocationEnabled(false);
    }
    
	private void populateMap() {
		final Hashtable<Marker, String> markers = new Hashtable<Marker, String>();
		
		final Iterator<Hashtable.Entry<MarkerOptions, String>> it = mData.markers.entrySet().iterator();
		while (it.hasNext()) {
			final Entry<MarkerOptions, String> next = it.next();
			markers.put(controls.map.addMarker(next.getKey()), next.getValue());
		}
		
		controls.map.setInfoWindowAdapter(new InfoWindowAdapter() {
			
			@Override
			public View getInfoWindow(final Marker marker) {
				if (markers.get(marker) != null) {
					final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService( Context.LAYOUT_INFLATER_SERVICE );

					final View view = inflater.inflate(R.layout.map_marker_layout, null);
					// Get the view parts
					final ImageView image = ((ImageView) view.findViewById(R.id.badge));
					final TextView titleUi = ((TextView) view.findViewById(R.id.obj_map_name));
					final TextView snippetUi = ((TextView) view.findViewById(R.id.obj_map_address));

					// Creation small thumbnail
					final int width = (int) getResources().getDimension(R.dimen.item_map_marker_dimension);
					final int height = (int) getResources().getDimension(R.dimen.item_map_marker_dimension);

					// Fill the view with data
					titleUi.setText(marker.getTitle());
					snippetUi.setText(marker.getSnippet());
					image.setImageBitmap(ImageUtils.resizeBitmapFromAbsolutePath(markers.get(marker), width, height));

					return view;
				}
				return null;
			}
			
			@Override
			public View getInfoContents(final Marker marker) {
				return null;
			}
		});
	}

	@Override
	public void onLoadingStarted() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLoadingFinished() {
		// TODO Auto-generated method stub
		
	}
}