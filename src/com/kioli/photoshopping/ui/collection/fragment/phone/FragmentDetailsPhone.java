package com.kioli.photoshopping.ui.collection.fragment.phone;

import java.util.ArrayList;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kioli.photoshopping.PhotoShopApp;
import com.kioli.photoshopping.R;
import com.kioli.photoshopping.async.IHandler;
import com.kioli.photoshopping.async.ThreadManager;
import com.kioli.photoshopping.async.operations.FindPathRunnable;
import com.kioli.photoshopping.component.SettingsToggle.TRANSPORTATION;
import com.kioli.photoshopping.model.ModelFragmentDetails;
import com.kioli.photoshopping.ui.collection.fragment.FragmentDetails;
import com.kioli.photoshopping.ui.collection.fragment.interfaces.ILoadingObserver;

public class FragmentDetailsPhone extends FragmentDetails implements ILoadingObserver {

    private static final class Controls {
		ImageView 	image;
		TextView 	name;
		TextView 	price;
		TextView 	address;
		TextView 	way;
		GoogleMap	map;
		FrameLayout	map_layout;
		Button 		edit_btn;
		Button 		delete_btn;
		Button 		shop_btn;
		LinearLayout spinner;
    }

    private static final String MODEL_ARGS_KEY = "model_details";

    private SupportMapFragment 		fragment;
    private ModelFragmentDetails    mData;
    private Controls         		controls;
    private boolean         		showSpinner = false;
    
	private final int zoomLevel = 15;
    
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        }
    }
    
    @Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		createMap();
	}
    
    @Override
	public void onResume() {
		super.onResume();
		if (controls.map == null) {
			controls.map = fragment.getMap();
	        centerAndDisableMap();
	        createCenterShopButton();
		}
	}
    
    private void restoreInstanceState(final Bundle state) {
        mData = state.getParcelable(MODEL_ARGS_KEY);
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        outState.putParcelable(MODEL_ARGS_KEY, mData);
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.frg_collection_details, container, false);
	    
        controls = new Controls();

        initViews(view);
        updateControls();
        populate();

        return view;
    }

    private void initViews(final View view) {
    	controls.image = (ImageView) view.findViewById(R.id.photo_image);
		controls.name = (TextView) view.findViewById(R.id.name_text);
		controls.price = (TextView) view.findViewById(R.id.price_text);
		controls.way = (TextView) view.findViewById(R.id.map_container_title);
		controls.address = (TextView) view.findViewById(R.id.address_text);
		controls.edit_btn = (Button) view.findViewById(R.id.photo_edit_btn);
		controls.delete_btn = (Button) view.findViewById(R.id.photo_delete_btn);
		controls.map_layout = (FrameLayout) view.findViewById(R.id.map_container);
		controls.spinner = (LinearLayout) view.findViewById(R.id.spinner);
		
		controls.edit_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				//TODO something
			}
		});
		
		controls.delete_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
					mBridge.onDeleteButtonClicked(mData.id);
			}
		});
    }
    
    @Override
    public void onLoadingStarted() {
        showSpinner = true;
        updateControls();
    }

    @Override
    public void onLoadingFinished() {
        showSpinner = false;
        updateControls();
    }

    private void showSpinner(final boolean toShow) {
    	if (toShow) {
    		controls.spinner.setVisibility(View.VISIBLE);
    	} else {
    		controls.spinner.setVisibility(View.GONE);
    	}
    }
    
    private void updateControls() {
        if(controls != null) {
            showSpinner(showSpinner);
        }
    }

    public void setData(final ModelFragmentDetails model) {
        mData = model;
        if(controls != null) {
            populate();
        }
    }
    
    private void populate() {
        controls.image.setImageBitmap(mData.photo);
        controls.name.setText(mData.name);
        controls.price.setText(mData.price.toStringWithCurrency());
        controls.address.setText(mData.address);
        controls.way.setText(R.string.photo_details_default_path);
    }

    private void createMap() {
    	final FragmentManager fm = getChildFragmentManager();
		fragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
		if (fragment == null) {
			fragment = SupportMapFragment.newInstance();
			fm.beginTransaction().replace(R.id.map_container, fragment).commit();
		}
    }
    
    private void createCenterShopButton() {
    	controls.shop_btn = new Button(getActivity());
    	
    	final FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.gravity=Gravity.BOTTOM | Gravity.LEFT;
            
		controls.shop_btn.setLayoutParams(params);   
		controls.shop_btn.setText(getString(R.string.photo_center_shop_btn));
		controls.shop_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				final CameraPosition cameraPosition = new CameraPosition.Builder().target(
		                new LatLng(mData.latitude, mData.longitude)).zoom(zoomLevel).build();
				controls.map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
			}
		});
		controls.map_layout.addView(controls.shop_btn);
    }
    
    private void centerAndDisableMap() {
    	final LatLng point = new LatLng(mData.latitude, mData.longitude);
    	final LatLng pointHome = PhotoShopApp.getProfile().homeLocation;
    	
    	// Add marker object
        final MarkerOptions marker_obj = new MarkerOptions().position(point);
		marker_obj.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        controls.map.addMarker(marker_obj);

        controls.map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mData.latitude, mData.longitude), zoomLevel));
        controls.map.setMyLocationEnabled(false);

        if (pointHome != null && point != null) {
            // Add marker house
            final MarkerOptions marker_home = new MarkerOptions().position(pointHome);
            marker_home.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_home));
            controls.map.addMarker(marker_home);
            
        	runCreatePath(pointHome, point, PhotoShopApp.getProfile().transportation.getValue());
        }
        
        final UiSettings settings = controls.map.getUiSettings();
        settings.setAllGesturesEnabled(false);
        settings.setMyLocationButtonEnabled(false);
        settings.setRotateGesturesEnabled(false);
        settings.setScrollGesturesEnabled(true);
        settings.setTiltGesturesEnabled(false);
        settings.setZoomGesturesEnabled(false);
        settings.setZoomControlsEnabled(true);
    }
    
	private void runCreatePath(final LatLng pointFrom, final LatLng pointTo, final String transportation) {
		ThreadManager.INSTANCE.execute(new FindPathRunnable(pointFrom, pointTo, transportation), new IHandler<ArrayList<LatLng>>() {
			@Override
			public void onSuccess(final ArrayList<LatLng> directionPoint) {
				final PolylineOptions rectLine = new PolylineOptions().width(10).color(Color.RED);

				for (int i = 0; i < directionPoint.size(); i++) {
					rectLine.add(directionPoint.get(i));
				}
				
				if (rectLine.getPoints().isEmpty()) {
					controls.way.setText(R.string.photo_details_no_path);
				} else {
					// Adding route on the map
					controls.map.addPolyline(rectLine);
					if (mData.transportation==TRANSPORTATION.DRIVING) {
						controls.way.setText(R.string.photo_details_car_path);
					} else {
						controls.way.setText(R.string.photo_details_walking_path);
					}
				}
			}
		});
	}
}
