package com.kioli.photoshopping.ui.collection.fragment.phone;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kioli.photoshopping.R;
import com.kioli.photoshopping.model.ModelFragmentDetails;
import com.kioli.photoshopping.model.ModelFragmentList;
import com.kioli.photoshopping.model.ModelFragmentMap;
import com.kioli.photoshopping.ui.collection.FLOW_STEP;
import com.kioli.photoshopping.ui.collection.fragment.FragmentDetails;
import com.kioli.photoshopping.ui.collection.fragment.FragmentList;
import com.kioli.photoshopping.ui.collection.fragment.FragmentMap;
import com.kioli.photoshopping.ui.collection.fragment.UiBridgeFragment;
import com.kioli.photoshopping.utils.ParcelableUtils;
import com.kioli.photoshopping.utils.support.SpinnerSupport;

public class UiBridgePhone extends UiBridgeFragment implements
		FragmentList.IBridge, FragmentMap.IBridge, FragmentDetails.IBridge {

    private static final class State implements Parcelable {
        public static final String PARCEL_ID = State.class.getName();

        public SpinnerSupport spinnerSupport;
        public FLOW_STEP     currentStep;

        public State() {
            currentStep   = FLOW_STEP.IDLE;
            spinnerSupport = new SpinnerSupport();
        }

        private State(final Parcel in) {
            currentStep   = ParcelableUtils.readEnum(in, FLOW_STEP.class);
            spinnerSupport = new SpinnerSupport(in);
        }

        @Override
        public void writeToParcel(final Parcel out, final int flags) {
            ParcelableUtils.writeEnum(out, currentStep);
            spinnerSupport.writeState(out, flags);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @SuppressWarnings("unused") // used by android
        public static final Creator<State> CREATOR = new Creator<State>() {
                   @Override
                   public State[] newArray(final int size) {
                       return null;
                   }

                   @Override
                   public State createFromParcel(final Parcel in) {
                       return new State(in);
                   }
               };
    }

    private static final String FRAGM_LIST_TAG = "fragment_list";
    private static final String FRAGM_MAP_TAG = "fragment_map";
    private static final String FRAGM_DETAILS_TAG = "fragment_details";

    private State mState;

    public UiBridgePhone() {
        super();
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Restore the saved state of the controller.
         * The data models in the fragments should be updated at a later point after this director is attached to
         * a controller, e.g. in onResume. */
        final boolean isRestoringInstanceState = (savedInstanceState != null);
        if (isRestoringInstanceState) {
            restoreInstanceState(savedInstanceState);
        } else {
            mState = new State();
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
    	return inflater.inflate(R.layout.frg_ui_bridge_phone, container, false);
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            initFragment(mState.currentStep, null);
        }
    }

    private void initFragment(final FLOW_STEP curStep, final Object initData) {
        final FragmentManager frman = getFragmentManager();
        final SpinnerSupport ss = mState.spinnerSupport;

        switch (curStep) {
        case LIST:
            FragmentListPhone fragmentList = (FragmentListPhone)frman.findFragmentByTag(FRAGM_LIST_TAG);
            if(fragmentList == null) {
            	fragmentList = new FragmentListPhone();
            	fragmentList.setData((ModelFragmentList) initData);
                frman.beginTransaction().replace(R.id.fragment_step_container, fragmentList, FRAGM_LIST_TAG).commit();
                frman.executePendingTransactions();
            }
            
            fragmentList.setController(this);
            fragmentList.setFlowDelegate(mFlow);
            ss.addObserver(fragmentList);
            break;

        case MAP:
            FragmentMapPhone fragmentMap = (FragmentMapPhone)frman.findFragmentByTag(FRAGM_MAP_TAG);
            if(fragmentMap == null) {
            	fragmentMap = new FragmentMapPhone();
            	fragmentMap.setData((ModelFragmentMap) initData);
                frman.beginTransaction().replace(R.id.fragment_step_container, fragmentMap, FRAGM_MAP_TAG).commit();
                frman.executePendingTransactions();
            }

            fragmentMap.setController(this);
            fragmentMap.setFlowDelegate(mFlow);
            ss.addObserver(fragmentMap);
            break;
            
        case DETAILS:
            FragmentDetailsPhone fragmentDetails = (FragmentDetailsPhone)frman.findFragmentByTag(FRAGM_DETAILS_TAG);
            if(fragmentDetails == null) {
            	fragmentDetails = new FragmentDetailsPhone();
            	fragmentDetails.setData((ModelFragmentDetails) initData);
                frman.beginTransaction().replace(R.id.fragment_step_container, fragmentDetails, FRAGM_DETAILS_TAG).commit();
                frman.executePendingTransactions();
            }

            fragmentDetails.setController(this);
            fragmentDetails.setFlowDelegate(mFlow);
            ss.addObserver(fragmentDetails);
            break;

        case IDLE:
            // Do nothing
            break;
        default:
            break;
        }
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        outState.putParcelable(State.PARCEL_ID, mState);
        super.onSaveInstanceState(outState);
    }

    private void restoreInstanceState(final Bundle savedState) {
        mState = savedState.getParcelable(State.PARCEL_ID);
    }

    /* ************** GENERAL NAVIGATION ************** */
    
    @Override
    public void goTo(final Object dataIn, final FLOW_STEP step) {
    	mState.currentStep = step;
    	
        switch (step) {
        case IDLE: 
        	throw new UnsupportedOperationException("Cannot go to this step " + FLOW_STEP.IDLE);
        
        case LIST:
            final ModelFragmentList modelList = (ModelFragmentList)dataIn;
            initFragment(step, modelList);
            break;
        
        case MAP: 
            final ModelFragmentMap modelMap = (ModelFragmentMap)dataIn;
            initFragment(step, modelMap);
            break;
        
        case DETAILS:
            final ModelFragmentDetails modelDetails = (ModelFragmentDetails) dataIn;
            initFragment(step, modelDetails);
            break;
        
        default:
            throw new UnsupportedOperationException("Not yet implemented for step " + step);
        }
    }
    
    @Override
    public void goBack(final Object dataIn) {
        final FLOW_STEP curStep = mState.currentStep;
        final FLOW_STEP newStep;
        switch (curStep) {
        case IDLE:
            throw new IllegalStateException("Not started");

        case LIST:
        	mFlow.finishFlow();
            newStep = FLOW_STEP.IDLE;
            break;

        case MAP:
        	final ModelFragmentList modelMap = (ModelFragmentList)dataIn;
            newStep = FLOW_STEP.LIST;
            initFragment(newStep, modelMap);
        	break;
        	
        case DETAILS:
        	final ModelFragmentList modelList = (ModelFragmentList)dataIn;
            newStep = FLOW_STEP.LIST;
            initFragment(newStep, modelList);
            break;

        default:
            throw new UnsupportedOperationException("Not yet implemented for step " + curStep);
        }

        mState.currentStep = newStep;
    }
    
    @Override
    public void onLoadingStarted() {
        mState.spinnerSupport.onLoadingStarted();
    }

    @Override
    public void onLoadingFinished() {
        mState.spinnerSupport.onLoadingFinished();
    }

	@Override
	public void onEditButtonClicked() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onItemSelected(int position) {
		mController.onItemSelected(position);
	}
	
	@Override
	public void onItemDeleted(int position) {
		mController.onItemDeleted(position);
	}
	
	@Override
	public void onDeleteButtonClicked(int position) {
		onItemDeleted(position);
		mController.goBack();
	}
	
	@Override
	public void onMapSelected() {
		mController.onMapSelected();
	}

}
