package com.kioli.photoshopping.ui.collection.fragment.interfaces;

import com.kioli.photoshopping.ui.collection.FLOW_STEP;
import com.kioli.photoshopping.ui.collection.FlowController;

public interface IUiBridge {

    void setController(FlowController controller);
    void setFlowDelegate(IFlow flowDelegate);

    /* ************** GENERAL NAVIGATION ************** */
    void goTo(Object dataIn, FLOW_STEP step);
    void goBack(Object dataIn);

    /* ************** DB OPERATIONS SUPPORT ************** */
    void onLoadingStarted();
    void onLoadingFinished();
    
}
