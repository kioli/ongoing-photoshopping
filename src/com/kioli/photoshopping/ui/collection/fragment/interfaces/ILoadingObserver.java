package com.kioli.photoshopping.ui.collection.fragment.interfaces;

/** A fragment may implement this interface if it wants to receive notifications. */
public interface ILoadingObserver {
    public void onLoadingStarted();
    public void onLoadingFinished();
}