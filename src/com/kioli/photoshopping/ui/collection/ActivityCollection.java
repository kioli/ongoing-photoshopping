package com.kioli.photoshopping.ui.collection;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.kioli.photoshopping.R;
import com.kioli.photoshopping.db.ObjectManager;
import com.kioli.photoshopping.model.ModelItemShopping;
import com.kioli.photoshopping.ui.collection.fragment.interfaces.IFlow;
import com.kioli.photoshopping.ui.collection.fragment.interfaces.IUiBridge;

public class ActivityCollection extends FragmentActivity implements IFlow {

	private List<ModelItemShopping> list_objects;
	
	private IUiBridge      mUiBridge;
    private FlowController mController;

	public static void start(final Activity starter) {
		final Intent intent = new Intent(starter, ActivityCollection.class);
		starter.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.act_collection);

        /* 1) Get the UI bridge */
        final FragmentManager frman = getSupportFragmentManager();
        mUiBridge = (IUiBridge) frman.findFragmentById(R.id.ui_bridge);

        /* 2) Create the flow controller*/
        mController = new FlowController(this, this);

        /* 3) Bind UI bridge and controller */
        mController.setUiBridge(mUiBridge);
        mUiBridge.setController(mController);

        /* Restore the controller's state after orientation change.
         * State of the UI bridge is restored automatically by the fragment manager. */
        if(savedInstanceState != null) {
            mController.restoreSavedInstanceState(savedInstanceState);
        } else {
            final ActivityCollection last = (ActivityCollection) getLastCustomNonConfigurationInstance();
            if (last == null) {
                mController.restoreLastCustomNonConfigurationInstance(null);
            } else {
                mController.restoreLastCustomNonConfigurationInstance(last.mController);
            }
        }
		
		list_objects = ObjectManager.getAllObjects();
	}
	
	@Override
    protected void onSaveInstanceState(final Bundle outState) {
        mController.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mController.isStarted() == false) {
            mController.start(list_objects);
        }
    }

    @Override
    public void onBackPressed() {
        mController.goBack();
    }

	@Override
	public void finishFlow() {
		finish();
	}
	
    public IUiBridge getmUiBridge() {
        return mUiBridge;
    }
}
