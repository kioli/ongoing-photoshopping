package com.kioli.photoshopping.ui.collection;

public enum FLOW_STEP {
    IDLE,
    LIST,
    DETAILS,
    MAP
}