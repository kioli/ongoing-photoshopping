package com.kioli.photoshopping.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.kioli.photoshopping.PhotoShopApp;
import com.kioli.photoshopping.R;
import com.kioli.photoshopping.adapter.MyTypesListAdapter;

public class ActivityListTypes extends Activity {

	private static final class Controls {
		ListView 		list;
		Button			add_type_button;
	}

    private Controls        		controls;
    private MyTypesListAdapter		adapter;
    
    public static void start(final Activity starter) {
		final Intent intent = new Intent(starter, ActivityListTypes.class);
		starter.startActivity(intent);
	}
    
    @Override
    public void onCreate(final Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
		setContentView(R.layout.act_object_list);

		controls = new Controls();

		initViews();
		populate();
    }
    
	private void initViews() {
		controls.list = (ListView) findViewById(R.id.list_object_types);
		controls.add_type_button = (Button) findViewById(R.id.add_object_type_btn);
		controls.add_type_button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
			}
		});
	}

    private void populate() {
    	adapter = new MyTypesListAdapter(this, PhotoShopApp.getProfile().object_types);
    	controls.list.setAdapter(adapter);
    }
	
//	public void onItemSelected(int position) {
//		ActivityUtils.hideSoftKeyboard(controls.search_collection);
//		mBridge.onItemSelected(position);
//	}
//	
//	public void onItemDeleted(int position) {
//		mBridge.onItemDeleted(position);
//		adapter.notifyDataSetChanged();
//	}
	
	public void onEmptyList(final boolean isListEmpty) {
		if (isListEmpty) {
			controls.list.setVisibility(View.INVISIBLE);
		} else {
			controls.list.setVisibility(View.VISIBLE);
		}
	}
}