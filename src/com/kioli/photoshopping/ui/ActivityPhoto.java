package com.kioli.photoshopping.ui;

import java.io.File;
import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kioli.photoshopping.PhotoShopApp;
import com.kioli.photoshopping.R;
import com.kioli.photoshopping.adapter.MyCoverviewAdapter;
import com.kioli.photoshopping.async.IHandler;
import com.kioli.photoshopping.async.ThreadManager;
import com.kioli.photoshopping.async.operations.CropPhotoRunnable;
import com.kioli.photoshopping.async.operations.GetAddressRunnable;
import com.kioli.photoshopping.async.operations.StoreItemRunnable;
import com.kioli.photoshopping.component.CoverFlow;
import com.kioli.photoshopping.db.ObjectManager;
import com.kioli.photoshopping.model.ModelItemShopping;
import com.kioli.photoshopping.model.ModelTypeObject;
import com.kioli.photoshopping.utils.DateUtils;
import com.kioli.photoshopping.utils.ImageUtils;
import com.kioli.photoshopping.utils.amount.Amount;
import com.kioli.photoshopping.utils.amount.AmountUtils;

public class ActivityPhoto extends Activity implements OnClickListener {

	private class Controls {
		ImageView	image;
		FrameLayout	image_layout;
		TextView	image_text;
		EditText	name;
		EditText	address;
		EditText	price;
		TextView	currency;
		Button		save_btn;
		Button		delete_btn;
		CoverFlow 	coverflow;
	}

	private static final int    CAMERA_REQUEST	= 1;
	private static final String SP_IMAGE		= "image_path";
	private static final String SP_FILE			= "image_file";

	private Controls	        controls;
	private File	        	image_file;
	private ModelItemShopping	item;

	public static void start(final Activity starter) {
		final Intent intent = new Intent(starter, ActivityPhoto.class);
		starter.startActivity(intent);
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_photo);

		controls = new Controls();
		item = new ModelItemShopping();

		initViews();
	}

	@Override
	protected void onResume() {
		super.onResume();
		PhotoShopApp.getTracker().getLocation();
	}

	@Override
	protected void onPause() {
		super.onPause();
		PhotoShopApp.getTracker().stopUsingGPS();
	}

	private void initViews() {
		controls.image = (ImageView) findViewById(R.id.photo_image);
		controls.image_layout = (FrameLayout) findViewById(R.id.photo_layout);
		controls.image_text = (TextView) findViewById(R.id.photo_text);
		controls.name = (EditText) findViewById(R.id.name_text);
		controls.address = (EditText) findViewById(R.id.address_text);
		controls.price = (EditText) findViewById(R.id.price_text);
		controls.currency = (TextView) findViewById(R.id.price_currency);
		controls.save_btn = (Button) findViewById(R.id.photo_save_btn);
		controls.delete_btn = (Button) findViewById(R.id.photo_delete_btn);
		controls.coverflow = (CoverFlow) findViewById(R.id.coverflow);

		controls.currency.setText(PhotoShopApp.getProfile().defaultCurrency.getSymbol());
		controls.price.setFilters(new InputFilter[] { new AmountUtils.AmountInputFilter(5) });

		controls.image_layout.setOnClickListener(this);
		controls.save_btn.setOnClickListener(this);
		controls.delete_btn.setOnClickListener(this);
		
		initCoverFlow();
	}
	
	private void initCoverFlow() {
		List<ModelTypeObject> list = PhotoShopApp.getProfile().object_types;
		MyCoverviewAdapter coverImageAdapter = new MyCoverviewAdapter(this, list);
		controls.coverflow.setAdapter(coverImageAdapter);

		controls.coverflow.setSpacing(0);
		controls.coverflow.setAnimationDuration(400);
		controls.coverflow.setOnItemSelectedListener(new OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            	//Do something with position
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// nothing
			}
		});
	}

	private void launchCamera() {
	    final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			createImageFile();
			takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(image_file));
			startActivityForResult(takePictureIntent, CAMERA_REQUEST);
		} else {
			Toast.makeText(ActivityPhoto.this, getString(R.string.photo_camera_not_present), Toast.LENGTH_SHORT).show();
		}
	}

	private void createImageFile() {
		try {
			if (image_file == null) {
				image_file = ImageUtils.createImageFile();
			}
		} catch (final IOException ex) {
			Toast.makeText(ActivityPhoto.this, getString(R.string.photo_file_not_created), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		if (requestCode == CAMERA_REQUEST) {
			if (resultCode == RESULT_OK) {
				controls.image_text.setText(R.string.photo_loading);
				runCropImage(image_file, (int) getResources().getDimension(R.dimen.details_item_photo_dimension));
				runGetAddress();
			}
		}
	}

	private void acquireData() {
		item.name = controls.name.getText().toString().trim();
		item.latitude = PhotoShopApp.getTracker().getLatitude();
		item.longitude = PhotoShopApp.getTracker().getLongitude();
		item.imgpath = image_file.getAbsolutePath();
		item.address = controls.address.getText().toString().trim();
		item.date = DateUtils.getNow();
		item.currency = PhotoShopApp.getProfile().defaultCurrency.getCode();
		item.price = Amount.fromStringValue(readPrice(controls.price.getText().toString()), item.currency);
		ObjectManager.saveOrUpdate(item);
	}

	private String readPrice(final String price) {
		return price.replaceAll("[^0-9]+", Amount.DECIMAL_DIVIDER);
	}

	private void runGetAddress() {
		ThreadManager.INSTANCE.execute(new GetAddressRunnable(this), new IHandler<String>() {
			@Override
			public void onSuccess(final String result) {
				if (controls.address.getText().length() == 0) {
					controls.address.setText(result);
				}

				if (result.length() == 0) {
					PhotoShopApp.getTracker().getLocation();
					runGetAddress();
				} else {
					PhotoShopApp.getTracker().stopUsingGPS();
				}
			}
		});
	}

	private void runCropImage(final File file, final int newDim) {
		controls.image.setVisibility(View.GONE);
		ThreadManager.INSTANCE.execute(new CropPhotoRunnable(file.getAbsolutePath(), newDim), new IHandler<Bitmap>() {
			@Override
			public void onSuccess(final Bitmap result) {
				controls.image.setVisibility(View.VISIBLE);
				controls.image.setImageBitmap(result);
				runStoreItem(result, image_file);
			}
		});
	}

	private void runStoreItem(final Bitmap bitmap, final File file) {
		ThreadManager.INSTANCE.execute(new StoreItemRunnable(bitmap, file), new IHandler<Boolean>() {
			@Override
			public void onSuccess(final Boolean result) {
				// Nothing
			}
		});
	}

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.photo_delete_btn:
			finish();
			break;
		case R.id.photo_save_btn:
			if (inputOk()) {
				acquireData();
				finish();
			}
			break;
		case R.id.photo_layout:
			launchCamera();
			break;
		default:
			throw new IllegalArgumentException("Invalid view called onClick");
		}
	}

	@Override
	protected void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		final SharedPreferences pref = PhotoShopApp.getPreferences();
		if(image_file != null) {
			final Editor prefsEditor = pref.edit();
			prefsEditor.putString(SP_IMAGE, image_file.getAbsolutePath());
			prefsEditor.commit();

			outState.putSerializable(SP_FILE, image_file);
		}
	}

	private boolean inputOk() {
		if (image_file == null) {
			Toast.makeText(this, getString(R.string.photo_missing_pic), Toast.LENGTH_SHORT).show();
			return false;
		} else if (controls.name.getText().toString().trim().length() == 0) {
			Toast.makeText(this, getString(R.string.photo_missing_name), Toast.LENGTH_SHORT).show();
			return false;
		} else if (controls.price.getText().toString().trim().length() == 0) {
			Toast.makeText(this, getString(R.string.photo_missing_amount), Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	@Override
	protected void onRestoreInstanceState(final Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		final String path = PhotoShopApp.getPreferences().getString(SP_IMAGE, "");
		controls.image.setImageBitmap(ImageUtils.getBitmapFromFile(path));
		image_file = (File) savedInstanceState.getSerializable(SP_FILE);
	}

	public static void cleanSharedPref() {
		final Editor prefsEditor = PhotoShopApp.getPreferences().edit();
		prefsEditor.remove(SP_IMAGE);
		prefsEditor.commit();
	}
}
