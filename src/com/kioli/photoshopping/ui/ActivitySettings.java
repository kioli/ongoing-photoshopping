package com.kioli.photoshopping.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kioli.photoshopping.PhotoShopApp;
import com.kioli.photoshopping.R;
import com.kioli.photoshopping.adapter.MySpinnerAdapter;
import com.kioli.photoshopping.animation.GeneralAnimations;
import com.kioli.photoshopping.component.SettingsToggle;
import com.kioli.photoshopping.component.SettingsToggle.IToggleCallback;
import com.kioli.photoshopping.component.SettingsToggle.TRANSPORTATION;
import com.kioli.photoshopping.model.ModelProfile;
import com.kioli.photoshopping.utils.amount.CURRENCY;

public class ActivitySettings extends Activity implements IToggleCallback, OnClickListener, OnCheckedChangeListener {

	private class Controls {
		Spinner currency;
		TextView address;
		GoogleMap	map;
		LinearLayout	map_layout;
		Button remove_home_btn;
		Button set_home_btn;
		Button center_home_btn;
		Button object_types;
		SettingsToggle transport_toggle;
		CheckBox address_street;
		CheckBox address_postcode;
		CheckBox address_country;
	}

	private ModelProfile profile;
	private Controls controls;

	private static final int zoomLevel = 14;
	private static final int cameraDelay = 1000;


	public static void start(final Activity starter) {
		final Intent intent = new Intent(starter, ActivitySettings.class);
		starter.startActivity(intent);
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_settings);

		controls = new Controls();
		profile = PhotoShopApp.getProfile();

		initViews();
		createMap();
	}

	private void initViews() {
		// CURRENCY SPINNER
		controls.currency = (Spinner) findViewById(R.id.settings_currency);

		final List<String> currencies = new ArrayList<String>();
		for (final CURRENCY currency : CURRENCY.values()) {
			currencies.add(currency.getCode() + " - " + currency.getSymbol());
		}
		controls.currency.setAdapter(new MySpinnerAdapter(this, currencies));
		controls.currency.setSelection(profile.defaultCurrency.ordinal());
		controls.currency.setOnItemSelectedListener(new MyOnItemSelectedListener());

		// MAP CONTAINER AND CLICKABLE BAR
		controls.map_layout = (LinearLayout) findViewById(R.id.settings_map_layout);

		controls.address = (TextView) findViewById(R.id.settings_home);
		controls.address.setOnClickListener(new View.OnClickListener() {

	        @Override
	        public void onClick(final View v) {
	            if (controls.map_layout.getVisibility() == View.GONE) {
	                GeneralAnimations.expand(controls.map_layout, getResources().getDimensionPixelSize(R.dimen.map_height_settings));
	            } else {
	            	GeneralAnimations.collapse(controls.map_layout, getResources().getDimensionPixelSize(R.dimen.map_height_settings));
	            }
	        }
	    });
		
		controls.center_home_btn = (Button) findViewById(R.id.settings_map_btn_locate);
		controls.set_home_btn = (Button) findViewById(R.id.settings_map_btn_home);
		controls.remove_home_btn = (Button) findViewById(R.id.settings_map_btn_remove);
		controls.center_home_btn.setOnClickListener(this);
		controls.set_home_btn.setOnClickListener(this);
		controls.remove_home_btn.setOnClickListener(this);

		controls.object_types = (Button) findViewById(R.id.settings_types_btn);
		controls.object_types.setOnClickListener(this);

		setHomePresentText();

        // TOGGLE FOR TRANSPORTATION METHOD
        controls.transport_toggle = (SettingsToggle) findViewById(R.id.settings_transport_toggle);
        controls.transport_toggle.setCallback(this);
        setToggleBg();
        
        setAddressCheckboxes();
	}
	
	private void setAddressCheckboxes() {
        controls.address_street = (CheckBox) findViewById(R.id.settings_check_address_street);
        controls.address_postcode = (CheckBox) findViewById(R.id.settings_check_address_postcode);
        controls.address_country = (CheckBox) findViewById(R.id.settings_check_address_country);
        
        controls.address_street.setChecked(profile.address_street);
        controls.address_postcode.setChecked(profile.address_postcode);
        controls.address_country.setChecked(profile.address_country);
        
        controls.address_street.setOnCheckedChangeListener(this);
        controls.address_postcode.setOnCheckedChangeListener(this);
        controls.address_country.setOnCheckedChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		PhotoShopApp.setProfile(profile);
	}

	public class MyOnItemSelectedListener implements OnItemSelectedListener {
		@Override
	    public void onItemSelected(final AdapterView<?> parent, final View view, final int pos, final long id) {
	    	profile.defaultCurrency = CURRENCY.values()[pos];
	    	((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.btn_txt));
	    }

		@Override
	    public void onNothingSelected(final AdapterView<?> parent) {
	        // Do nothing.
	    }
	}

	private void createMap() {
		if (controls.map == null) {
			controls.map = ((MapFragment) getFragmentManager().findFragmentById(R.id.settings_map)).getMap();
            if (controls.map == null) {
                Toast.makeText(getApplicationContext(),"Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            } else {
            	setMapListener();
            	checkHomeAlreadyPresent();
            }
        }
    }

	private void setMapListener() {
		controls.map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(final LatLng point) {
            	profile.homeLocation = point;
            	controls.map.clear();
                controls.map.addMarker(new MarkerOptions().position(point).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_home)));
                setHomePresentText();
            }
        });
	}

	private void checkHomeAlreadyPresent() {
		if (profile.homeLocation != null) {
			controls.map.addMarker(new MarkerOptions().position(profile.homeLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_home)));
			controls.map.animateCamera(
					CameraUpdateFactory.newCameraPosition(CameraPosition
							.builder().target(profile.homeLocation).zoom(zoomLevel)
							.build()), cameraDelay, null);
    		setHomePresentText();
    	}
	}

	private void setHomePresentText() {
		if (profile.homeLocation != null) {
			((TextView) findViewById(R.id.settings_home_is_set)).setText(R.string.settings_address_set);
    	} else {
    		((TextView) findViewById(R.id.settings_home_is_set)).setText(R.string.settings_address_nonSet);
    	}
	}

	private void setToggleBg() {
		switch (profile.transportation) {
		case DRIVING:
			controls.transport_toggle.drivingSelected();
			break;
		default:
		case WALKING:
			controls.transport_toggle.walkingSelected();
			break;
		}
	}

	@Override
	public void onToggleCLicked(final TRANSPORTATION transport) {
		profile.transportation = transport;
	}

	@Override
	public void onBackPressed() {
		if (controls.map_layout.getVisibility() == View.VISIBLE) {
			GeneralAnimations.collapse(controls.map_layout, getResources().getDimensionPixelSize(R.dimen.map_height_settings));
        } else {
        	super.onBackPressed();
        }
	}

	public TRANSPORTATION getTransportation() {
		return profile.transportation;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.settings_map_btn_home:
			checkHomeAlreadyPresent();
			break;
		case R.id.settings_map_btn_remove:
			profile.homeLocation = null;
			controls.map.clear();
			setHomePresentText();
			break;
		case R.id.settings_map_btn_locate:
			controls.map.clear();
			final Location here = PhotoShopApp.getTracker().getLocation();
			profile.homeLocation = new LatLng(here.getLatitude(), here.getLongitude());
			checkHomeAlreadyPresent();
			break;
		case R.id.settings_types_btn:
			ActivityListTypes.start(this);
			break;
		default:
			break;
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
		case R.id.settings_check_address_street:
			profile.address_street = isChecked;
			break;
		case R.id.settings_check_address_postcode:
			profile.address_postcode = isChecked;
			break;
		case R.id.settings_check_address_country:
			profile.address_country = isChecked;
			break;
		default:
			break;
		}
	}
}
