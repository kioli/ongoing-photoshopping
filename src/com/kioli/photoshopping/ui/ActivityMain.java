package com.kioli.photoshopping.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;

import com.kioli.photoshopping.PhotoShopApp;
import com.kioli.photoshopping.R;
import com.kioli.photoshopping.ui.collection.ActivityCollection;

public class ActivityMain extends Activity {

	private class Controls {
		FrameLayout btn_camera;
		FrameLayout btn_gallery;
		FrameLayout btn_settings;
	}

	private Controls controls;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_main);

		controls = new Controls();

		initViews();

		PhotoShopApp.checkRatingApp(this);
	}

	private void initViews() {
		controls.btn_camera = (FrameLayout) findViewById(R.id.start_photo);
		controls.btn_gallery = (FrameLayout) findViewById(R.id.start_gallery);
		controls.btn_settings = (FrameLayout) findViewById(R.id.start_settings);

		controls.btn_camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				ActivityPhoto.start(ActivityMain.this);
			}
		});

		controls.btn_gallery.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				ActivityCollection.start(ActivityMain.this);
			}
		});

		controls.btn_settings.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				ActivitySettings.start(ActivityMain.this);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		ActivityPhoto.cleanSharedPref();
	}
}
